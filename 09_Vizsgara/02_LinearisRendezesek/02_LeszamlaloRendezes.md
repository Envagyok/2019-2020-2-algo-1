# Leszámláló rendezés (Counting Sort)

## Elv

* Adott egy a : T[n] tömb, azaz egy a nevű T-ket tartalmazó n elemű tömb, amit 0-tól n-1-ig indexelünk
* Ezt szeretnénk lineáris időben rendezni egy hasonló méretű és alakú b nevű tömbbe
* Azaz ez a rendezés nem helyben rendez, viszont stabil
* Feltétele, hogy legyen adott egy h : T → [0..k] olcsón kiszámítható, T-t egyenletesen elosztó leképezés, ahol k egy természetes szám és jóval kisebb, mint n (másképp fogalmazva: k ∈ Ο(n))
* h tulajdonképpen egy hash-függvény lesz (lásd ezután), aminek a dolga, hogy szortírozza az input elemeit a 0-tól k-ig indexelt, összesen tehát k+1 db edénybe
* Itt most a T a kulcsok halmaza, ezek a kulcsok bármik lehetnek, egész addig, amíg létezik ez a leképezés
* Ez az előfeltétel tulajdonképpen olyanná teszi T elemeit, mint az előadáson hallott nem számjegypozíciós edényrendezés inputja -- azaz nem elvárás, hogy a kulcs hierarchikus legyen, viszont azt elvárjuk, hogy diszkrét értékei legyenek egyenletes eloszlással
* Ekkor a következőt tehetjük:
    * Létrehozunk egy pos nevű segédtömböt, ami 0-tól van indexelve k-ig (azaz k+1 elemes) és természetes számokat tartalmaz (azaz pos : N[k+1])
    * Elemeit 0-ra inicializáljuk
    * Végigolvassuk az inputot, mindegyik elemen rendre meghívjuk h-t, amit a függvény visszaad, a pos annyiadik indexét eggyel megnöveljük - azaz megszámláljuk h melyik értékéből mennyi van
    * A végén a pos tömb elemeit kumuláljuk, azaz sorban haladva a pos minden eleméhez hozzáadjuk a megelőző elem értékét (avagy a pos elemeiben a kumulálás előtti pos elemeinek kezdőrészlet-összege lesz)
        * Nyilván ezt a ciklust elég 1-től (a második elemtől) indítani, mert a pos[0] nem változik
    * Most a posban nem az lesz, hogy az egyes h értékek hányan vannak, hanem ha egymás mögé rakjuk őket rendezve, hányadik pozíciónál kezdődne a következő érték spektruma (tehát ha pos[i] = j, akkor b[j]-be már az i+1 érték reprezentánsai kerülnek (vagy az i+2 értéké, ha nincs egyáltalán i+1-es, stb)
    * Ha ez megvan, még egyszer végigmegyünk az elemeken, de ezúttal fordított sorrendben (a stabilitás megtartása miatt). Újra meghívjuk h-t minden elemre, megnézzük, a pos annyiadik indexén milyen érték van: amit találunk, az az az index, ami az utolsó "jó" érték utáni elem, tehát előbb csökkentjük ezt a számot, majd b-nek a csökkentett indexedik helyére helyezzük át a feldolgozás alatt álló elemet. A csökkentés azért kell, hogy legközelebb már a most beszúrt elem elé pakoljunk (na, ezért marad meg a stabilitás)
    * A végén a post persze eldobhatjuk
* Az a-n (azaz a [0..n-1] intervallumon) kétszer, a poson (azaz a [0..k] intervallumon) egyszer megyünk végig, a h-kat 2n-szer számoljuk ki, de ez elemi műveletnek tekinthető, k pedig elhanyagolható n tükrében, így a rendezés ϴ(n)-es

## Lejátszás

* Legyen az input: a[n] = [12, 13, 00, 01, 32, 11]
* Ne zavarjon meg minket, hogy a elemei hierarchikus diszkrét elemű kulcsok, így a számjegypozíciós rendezésre is jók lennének, itt ezt nem használjuk ki, akár traktorok vagy playmate-ek is lehetnének a kulcsok...
* Legyen a h függvény, ami a (t,e) párhoz t-t rendel, vagy másképp, ami az e elemhez ⌊e/10⌋-et rendel, azaz ami visszaadja az első számjegyét (h(12) = 1, stb)
* Ez alapján erre az inputra k = 3 az ideális választás -- ezt persze paraméterként kapjuk, nem az inputból nézzük ki
* Az alábbi táblázat értelmezése:
    * A fejlécen kívüli sorai pos indexei
    * Az oszlopok az idő múlását, azaz az algoritmus lépéseit jelölik (tehát minden oszlop pos tartalmának egy pillanatképe)
    * Az INIT résznél hozzuk létre post csupa 0 értékkel
    * Az egyes számozott körökben az a i. eleme h szerinti képét, mint pos-beli indexet számoljuk ki és növeljük a hozzá tartozó értéket -- az üresen maradó cellákat úgy értjük, hogy az értékük az maradt, ami az előző körben volt
    * A SUM oszlopban áttekintjük mi lett pos állapota a számlálás után
    * A KUM oszlopban áttekintjük, mi lett pos állapota a kumulálás után
    * Majd újra végigmegyünk a [0..n-1] intervallumon, ám ezúttal visszafelé
    * Közben építjük b-t -- csökkentjük az aktuális h(a[i])-edik elemét posnak, majd az ennyiedik b-beli elemként felvesszük a[i]-t, (pos csökkentett értékét a táblázatba írjuk)
* Ne feledjük el b végeredményét is leírni a táblázat alatt

```
       INIT 0.  1.  2.  3.  4.  5.  SUM KUM 5.  4.  3.  2.  1.  0.
    0  0            1   2           2   2           1   0
    1  0    1   2               3   3   5   4               3   2
    2  0                            0   5
    3  0                    1       1   6       5

    b=[00, 01, 12, 13, 11, 32]
```

* Láthatjuk, b elemei rendezettek az első számjegy szerint, az "egyenlőnek" értékelt elemek között pedig a sorrend az a-beli eredeti sorrend -- azaz a rendezés stabil
* Az, ahogy említettem, ne zavarjon meg, hogy b-ben nem teljesen sorrendben vannak a számok -- a rendezési relációnk most ebben a példában csak az első helyiértéket vette figyelembe: a 13 és a 12 ekvivalensnek számít a hash szempontjából