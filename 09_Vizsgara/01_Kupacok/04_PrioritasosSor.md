# Prioritásos sor

## Alapok

* Priority Queue
* Egy olyan adatszerkezet, amiben párokat (a tényleges berakandó elemek + prioritásuk) tárolunk
* Nem LIFO, sem FIFO, a berakás a sor "végére" történik, de a kivenni mindig a legnagyobb prioritású még bennlévő elemet fogjuk
* Létezik MinQ és MaxQ, attól függően, a legkisebb, vagy a legnagyobb prioritást keressük. A default a MaxQ, hacsak nem mondunk mást, PrQueue néven erre gondolunk

## Interfésze

* PrQueue()
* add(e : T,p : N)
* remMax() : T, remMax(&x : T)
* max() : T, max(&x : T)
* isEmpty() : L, isEmpty(&l : L)
* isFull() : L, isFull(&l : L)
* Opcionálisan a prioritásokat újraszámoló függvény, amennyiben ezek változhatnak (lásd jövőre Dijkstra-algoritmus)

## Reprezentáció

* 3 féle gyakori megvalósítás van, a lelke mélyén mindhárom tömbös:
	* Rendezetlen tömb
		* add() - végére: Θ(1)
		* remMax() - maximumkiválasztás tétel: Θ(n)
	* Rendezett tömb
		* add() - rendezett beszúrás: Θ(n)
		* remMax() - utolsó elem: Θ(1)
	* Kb. rendezetlen tömb - kupac
		* add() - lásd lentebb: Θ(log(n))
		* remMax() - már láttuk a kupacrendezésnél. Kicserélem az elsőt és az utolsót, csökkentem a darabot, majd süllyesztek - az első kettő konstans, a harmadik a domináns: Θ(log(n))
* Látható, hogy az, hogy melyik reprezentáció az ajánlott, attól függ, milyen gyakran várható hozzáadás és kivétel

## Az add() művelet lejátszása kupacra

```
     17
    /  \
   9   13
  /\
 7  2
```

* Ez a kupac ennek a tömbnek felel meg: [17, 9, 13, 7, 2]
* Most hívjuk meg az add(x, 20)-at (ahol az x a berakandó elem, de ezt most nem is jelölöm, csak a 20-at, ami a prioritása)
* Rakjuk a vége után sorfolytonosan

	```
	     17
	    /  \
	   9   13
	  /\   /
	 7  2  20
	```
	
* Elromlott a kupac-tulajdonság, de biztosan csak egy útvonalon
* A beszúrt elemtől kiindulva felfelé hajtsuk végre az ún. emelést. Mindig cserélem a szülővel, ha nagyobb vagyok nála. Mivel nagyobb voltam a szülőnél, a másik gyerekénél is automatikusan nagyobb leszek, így a többi utat ténylegesen nem kell vizsgálni, innen a logaritmikus műveletigény

	```
	       20
	      /  \
	     9   17
	    /\   /
	   7  2  13
	```

## Prioritásos sor kupacos ábrázolása


### 0-tól indexelt tömbbel

* \- a : (T×N)[n], a tömb amiben vannak az elem--prió-párok
* \- db : [0..n], a tömb első ennyi eleme van kihasználva (azaz a db-1. indexig vagyunk feltöltve)

#### add() művelet kupacos ábrázolásra

```
PrQueue::add(p : N) // az egyszerűség kedvéért csak a prioritást szúrom be
  HA db = n
    HIBA
  KÜLÖNBEN
    a[db] := p
    raise(db)
    db := db+1
```

```
PrQueue::raise(index : N)
  parent := alsóegészrész((index-1)/2)
  HA parent > -1 && a[parent] < a[index]
    t := a[index]
    a[index] := a[parent]
    a[parent] := t
    raise(parent)
```

### 1-től indexelt tömbbel

* \- a/1 : (T×N)[n], a tömb amiben vannak az elem--prió-párok
* \- db : [0..n], a tömb maximális indexe, ami még ki van használva

#### add() művelet kupacos ábrázolásra

```
PrQueue::add(p : N) // az egyszerűség kedvéért csak a prioritást szúrom be
  HA db = n
    HIBA
  KÜLÖNBEN
    db := db+1
    a[db] := p
    raise(db)
```

```
PrQueue::raise(index : N)
  parent := alsóegészrész(index/2)
  HA parent > 0 && a[parent] < a[index]
    t := a[index]
    a[index] := a[parent]
    a[parent] := t
    raise(parent)
```
