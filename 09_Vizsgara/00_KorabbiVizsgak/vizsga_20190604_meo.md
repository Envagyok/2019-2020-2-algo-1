# 3. Algoritmusok és adatszerkezetek I. vizsga - 2019. 06. 04.
## Részleges megoldás

### 2. feladat

#### 12p - a) Adja meg a leszámláló rendezés előfeltételeit, struktogramját és aszimptotikus műveletigényét!

* 3p - Előfeltételek:
    * Az input legyen tömb (a : T[n])
    * Ne várjuk el a helyben rendezést
    * Akár elvárhatjuk a stabilitást
    * Legyen adott egy φ : T → [0..r-1] olcsón kiszámítható kulcsfüggvény, ahol r eleme N+ és r eleme Ο(n)
    * Érkeztek olyan megoldások, hogy "minden a bemenetet képező szám azonos »hosszú« legyen, amit d-vel jelölünk és azonos számrendszer feletti legyen, aminek az alapszáma r". Ez nem rossz, de nagyon leszűkíti a lehetőségeinket. A tömb alaptípusa -- T -- bármi lehet, amíg igaz, hogy van ilyen tulajdonságú függvény rá. Persze T lehet "a 3 számjegyből álló számok" halmaza is, a leképezés pedig lehet valamelyik számjegyre való projektálás. De az általános megoldás a fenti
* 6p - Struktogram:

```
countingSort(a : T[n], b : T[n], r : N)
	z : N[r]
	FOR k = 0 to r-1
		z[k] = 0
	FOR i = 1 to n
		z[φ(a[i])] = z[φ(a[i])] + 1
	FOR k = 1 to r-1
		z[k] = z[k] + z[k-1]
	FOR i = n down to 1
		k = φ(a[i])
		b[z[k]] = a[i]
		z[k] = z[k] - 1
```

* 2+1p - Műveletigény:
	* Első ciklus: kinullázzuk a z : [0..r-1]-es tömböt, ami r tulajdonságai miatt Θ(r) ~ Ο(n)
	* Második ciklus: végigmegyünk az inputon, leképezzük konstans időben a kulcsra, z így meghatározott értékét növeljük -- azaz szétszórjuk a megfelelő edényekbe az elemeket: Θ(n)
	* Harmadik ciklus: kumuláljuk z elemeit, ez megint Θ(r) ~ Ο(n)
	* Negyedik ciklus: végigmegyünk az inputon visszafelé, és mindig annyiadik elemébe írjuk b-be az adott elemet, ahova a z segédtömb szánja: Θ(n)
    * Összesen tehát ez Θ(n+r) ~ Θ(n) - a bemenet méretére lineáris

#### 8p - b) Szemléltesse a <30; 20; 11; 22; 23; 13> négyes számrendszerbeli számok tömbjén, ha a kulcsfüggvény a baloldali számjegyet választja ki!

* Az alábbi táblázat egyes sorai a z segédtömb [0..3] indexeken felvett értékeit mutatják az eltelt idő függvényében, amit a táblázat oszlopai alapján tudunk követni
    * Az "Init." oszlop az első, "kinullázós" ciklus eredményét mutatja, míg a számokkal jelölt oszlopok az a tömb indextartományán végigfutó 2. ciklus lépéseit adják meg - a 6. körre minden z-beli indexhez hozzárendeltük hány db olyan szám volt a-ban, aminek az volt az első számjegye
    * A "C" oszlopban a 3. ciklus által előállított kumulált értékek olvashatók, amik azt adják meg, a-ban mennyi olyan érték volt, aminek legfeljebb az adott index volt az első számjegye - másképpen fogalmazva: melyik az az index, ahol az utolsó i-vel kezdődő értéknek van helye a majdani rendezett tömbben
    * Ezt követően újra bejárjuk az eredeti tömb indextartományát, de most visszafelé, hogy az i. a-beli elemet berakhassuk b z[φa[i]]. helyére. Persze ekkor z[φa[i]]-t csökkentjük, hogy a következő ilyen elem már az eggyel korábbi indexre kerüljön, ne a frissen elfoglaltra
    * A b tömb értékei ezen ciklus során állnak elő
```
z | Init. 1 2 3 4 5 6 | C | 6 5 4 3 2 1
---------------------------------------
0 |   0   0 0 0 0 0 0 | 0 | 0 0 0 0 0 0
1 |   0   0 0 1 1 1 2 | 2 | 1 1 1 0 0 0
2 |   0   0 1 1 2 3 3 | 5 | 5 4 3 2 2 2
3 |   0   1 1 1 1 1 1 | 6 | 6 6 6 6 6 5

b = [11, 13, 20, 22, 23, 30]
```

#### 5p - c) Mi teljesül a bemenetre, és mi a rendezésre, hogy a fenti példában a végeredmény, mint számsor is rendezett lett? Hogyan biztosítottuk a rendezés e tulajdonságát?

* 1p - A bemenetre igaz, hogy amelyik számok a kulcsfüggvény szerint ekvivalensek (azaz, amelyikek azonos számjeggyel kezdődnek), azok speciel emelkedő sorrendben követik egymást (pl. a 20 után van a 22, azután a 23)
* 2p - A rendezés pedig stabil: azaz a kulcsfüggvény szerint ekvivalens elemek egymáshoz képest vett sorrendjét helyben hagyja
* 2p - Az utolsó ciklus fordított sorrendben járja be az elemeket s minden elemet a rendezés szerint neki szánható lehetséges, még feltöltetlen helyek legvégére rakja. Mivel hátulról töltjük fel az elemeket, és a ciklus is hátulról adagolja az elemeket, az eredeti sorrend nem változik

### 3. feladat

#### a) Definiálja a bináris keresőfa fogalmát, feltéve, hogy a bináris fa fogalma már ismert!

* Bináris keresőfának hívunk egy olyan bináris fát, aminek minden nem üres részfájára igaz, hogy annak gyökércsúcsának kulcsa nagyobb, mint bármelyik bal részfájában található csúcs kulcsa, s kisebb, mint bármelyik jobb részfájában levő csúcs kulcsa

#### b) Adott a t bináris fa. A csúcsok kulcsai pozitív egész számok. Írja meg a bst(t) logikai függvényt; ami a t egyszeri (Inorder) bejárásával eldönti, hogy keresőfa-e! MT(n) ∈ Ο(n), ahol n = |t|

* Ötlet: A bejárást és eldöntést a megfelelően inicializált, rekurzív, bst(t, k) logikai segédfüggvény végezze, ami híváskor k-ban a t kulcsainál kisebb értéket vár, visszatéréskor pedig, amennyiben t nemüres keresőfa, a t-beli legnagyobb kulcsot tartalmazza! Ha t üres, akkor k-ban maradjon a függvényhívásnál kapott érték!
```
bst(t : BinTree) : L
    HA t == ÜRES
        return true
    KÜLÖNBEN
        max = 0
        return bst(t, max)
```

* Nem t.key()-t, hanem egy segédváltozót adtam át, mert nem akartam, hogy a fa megváltozzon, amikor a referencia szerint átadott k-t módosítom

```
bst(t : BinTree, k& : N) : L
    HA t == ÜRES
        return true
    KÜLÖNBEN
        HA bst(t.left(), k)
            HA k >= t.key()
                return false
            KÜLÖNBEN
                k = t.key()
                return bst(t.right(), k)
        KÜLÖNBEN
            return false
```

#### c) Igaz-e az Ön által megfogalmazott bst(t) logikai függvényre, hogy mT(h) ∈ Ο(h), ahol h = h(t)? Miért?

* mT(h) ∈ Θ(1), mivel bármekkora is a fa (s így a magassága), ha a gyökér bal részfája csak egy csúcsos, és annak a csúcsnak rögtön nagyobb-egyenlő a kulcsa, az konstans lépésből kiderül
* De ami konstans, arra igaz, hogy Ο(h), tehát végül is az állítás igaz

### 4. feladat

* Tegyük fel, hogy g : N → R, aszimptotikusan pozitív függvény!

#### a) Adja meg az Ο(g) és az Ω(g) függvényhalmazok definícióját!

* Megj.: gyakon nem aszimptotikusan pozitívnak, hanem "végig" nemnegatívnak értelmeztük a műveletigényeket leíró függvényeket. Ez fölfogás kérdése, igazából se a negatív. se a 0 műveletigény nem életszerű. A definíciókban ezen kívül különbség nincs, a következményekben, s azok bizonyításainak nehézségében lehet
* Az aszimptotikusan pozitív azt jelenti, hogy az N → R alakú függvény egy bizonyos index után csupa pozitív értékeket vesz fel
* Ο(g) = {f : N → R | f aszimptotikusan pozitív és van olyan c > 0 és van olyan n<sub>0</sub> >= 0, hogy minden n >= n<sub>0</sub> : c * g(n) >= f(n)}
    * Azaz olyan függvények, amikre mind igaz, hogy egy indextől kezdve, egy ettől független pozitív konstans szorzóval g felülről becsli azokat
* Ω(g) = {f : N → R | f aszimptotikusan pozitív és van olyan c > 0 és van olyan n<sub>0</sub> >= 0, hogy minden n >= n<sub>0</sub> : c * g(n) <= f(n)}
    * Ekkor pedig g a függvényhalmazba tartozó függvények aszimptotikus alsó korlátja

#### b) Milyen alapvető összefüggést ismer az Ο(g), az Ω(g) és a Θ(g) függvényhalmazok között?

* Θ(g) = Ο(g) unió Ω(g)

#### c) Igaz-e, hogy (3n + 4)<sup>2</sup> ∈ Θ(n<sup>2</sup>)? Miért?

* Igaz, ugyanis:
    * (3n + 4)<sup>2</sup> = 9n<sup>2</sup> + 24n + 16
    * lim (n->végtelen) [9n<sup>2</sup> + 24n + 16] / [n<sup>2</sup>], ha mindkét oldalt n<sup>2</sup>-tel osztjuk:
    * lim (n->végtelen) [9 + 24/n + 16/n<sup>2</sup>] / [1],
    * A hányadosos tagok, ahogy n nő, csökkennek, tehát a 9 marad, ez az egész 9-be tart, ha n tart végtelenbe, ami egy konstans, így a Θ-viszony fennáll
* Könnyedén belátható a definícióból is, megfelelő c és n<sub>0</sub> konstansok felmutatásával

#### d) Igaz-e, hogy n<sup>n</sup> ∈ Ω(2<sup>n</sup>)? Miért?

* Igaz, ugyanis:
    * lim (n->végtelen) [n<sup>n</sup>] / [2<sup>n</sup>]
    * lim (n->végtelen) (n/2)<sup>n</sup>
    * Ez, ahogy n nő szintén nő, azaz a végtelenbe tart. Tétel szerint, ha a hányados határértéke végtelenbe tart, akkor a számlálóban levő függvény aszimptotikus felső becslése a nevezőbeli függvénynek. Az Ω-viszony épp ezt jelenti

#### e) Igaz-e, hogy 1000n<sup>2</sup>lg(n) ∈ O(n<sup>3</sup>)? Miért?

* Igaz, ugyanis:
    * lim (n->végtelen) [1000n<sup>2</sup>lg(n)] / [n<sup>3</sup>]
    * 1000 * lim (n->végtelen) [lg(n)] / [n]
    * Az 1000 konstans szorzó nem lényeges, n-ről pedig tudjuk (lineáris), hogy aszimptotikusan felső becslése a logaritmusfüggvénynek. Innen ez a határérték 0 lesz, azaz a nevezőben levő függvény aszimptotikus felső becslése a számlálóban levőnek. Az állítás pedig erről szólt