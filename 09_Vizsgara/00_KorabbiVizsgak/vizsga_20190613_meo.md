# Algoritmusok és adatszerkezetek - 2019. 06. 13. vizsga - Részleges megoldás

## 1. feladat

### 1.a, Mutassa be a számjegypozíciós ("Radix") rendezés működését a <31; 20; 11; 23; 21; 10> négyes számrendszerbeli számok listáján! Az egyes menetekben a megfelelő számjegy szerinti szétválogató rendezést alkalmazzon!

* Az input
    * Mérete - n = 6
	* Számainak számrendszer-alapszáma - r = 4
	* Számjegyek száma minden elemére - d = 2
* Ez alapján d = 2 menet lesz
	* Először a jobb oldali (kevésbé szignifikáns) számjegy szerint válogatunk szét négy edénybe/polcra, majd az így keletkezett listákat összefűzzük
	* Utána ezt megismételjük a bal számjeggyel is
* 1. menet:
	* 0-s edény: <20; 10>
	* 1-es edény: <31; 11; 21>
	* 2-es edény: <>
	* 3-as edény: <23>
* Összefűzve: <20; 10; 31; 11; 21; 23>
* 2. menet:
	* 0-s edény: <>
	* 1-es edény: <10; 11>
	* 2-es edény: <20; 21; 23>
	* 3-as edény: <31>
* Összefűzve: <10; 11; 20; 21; 23; 31>

#### Pontozás - 10p

* Vannak edények és [0..3]-ig vannak: 2p
* 2 menet van, jó sorrendben, valamilyen módon jelöli az akt. számjegyet: 3p
* Sorrendtartóan, az akt. számjegy szerint válogat szét mindkét esetben: 3p
* Összefűzés megvan mindkét esetben helyesen: 2p

### 1.b, Mekkora a Radix rendezés műveletigénye, és miért?

* Összesen d-szer megyünk végig az inputon (*d)
	* Elkészítjük az r darab listát, egyesével konstans időben (*r)
	* Minden körben mind az n elemen megyünk végig (*n)
		* Minden elem feldolgozásakor lekérjük annak adott számjegyét (konstans), és az annak megfelelő edénybe helyezzük, azaz egy végelemes lista végére szúrjuk (konstans) (*1)
	* A listákon ezután végigmegyünk, és összefűzzük azokat. Maga az összefűzés, ha a listákon hátulról előre megyünk, s mindig az aktuális lista végére fűzzük, az eddig elkészített listát, konstans; ezt a listák számaszor, azaz r-szer hajtjuk végre (*r)
* Ebből Θ(d * (2r + n)) adódik, ami ha feltesszük, hogy d eleme Θ(1) és r eleme ο(n), egyszerűsíthető Θ(n)-re

#### Pontozás - 8p
* Θ(n) / lineáris: 2p
* Θ(d * (2r+n)): önmagában, esetleg 2-es szorzó nélkül 2p, az előzővel együtt a kettő összesen 3p
* Ha le is írta (nem feltétlen a b) kérdéshez), hogy mi a d és r: 1p
* d és r viszonya n-hez: 1p
* Θ(d * (2r+n)) levezetése: 3p

### 1.c, A segédrendezés mely tulajdonságára épül a Radix rendezés, és miért?

* A segédrendezés - itt a "szétválogatás" - stabil, azaz a rendezés szerint ekvivalensnek vett elemeket egymáshoz képest az eredeti sorrendben tartja
* Első körben az utolsó számjegy szerint rendezünk. i eleme [2..d]-re hátulról az i. számjegy szerint rendezünk, de úgy, hogy az utolsó i-1 számjegy szerint már rendezve volt az input. Most, amelyik elemek azonosak az i. számjegyen, olyan sorrendben őrizzük meg, ahogy a korábbi rendezések hagyták, azaz rendezve. Amikor i=d, akkor megkapjuk, hogy a teljes lista rendezett

#### Pontozás - 7p
* "Stabil" szó: 2p
* Definíciója: 2p
* Indoklás (hátulról előre haladunk): 3p

## 4. feladat

### 4. A Z[0..(m−1)] hasító tábla rései kétirányú, nemciklikus, fejelem nélküli, rendezetlen láncolt listák pointerei. Adott a k mod m hasító függvény. A kulcsütközéseket láncolással oldjuk fel. Mindegyik kulcs csak egyszer szerepelhet Z-ben.
### 4.a, Írja meg az ins(Z, k, a):0..2 értékű függvényt, ami beszúrja a hasító táblába a (k, a) kulcs-adat párt! Ha a táblában már volt k kulcsú elem, a beszúrás meghiúsul, és a 2 hibakódot adja vissza. Különben, ha nem tudja már a szükséges listaelemet allokálni, az 1 hibakódot adja vissza. (Feltesszük, hogy a new művelet, ha sikertelen, akkor NULL pointert ad vissza.) Az ins() művelet akkor ad vissza 0 kódot, ha sikeres volt a beszúrás. Ilyenkor az új listaelemet a megfelelő lista elejére szúrja be.

* A hash-táblában kétirányú, nemciklikus listák vannak, azaz E2 típusú elemekre mutató pointerek
* E2 adattagjai itt és most:
	* key : Z
	* data : D (D tetsz. típus)
	* next : E2*
	* prev : E2*
```
ins(z : E2*[m], k : Z, a : D) : [0..2]
	i = k mod m
	p = z[i]
	AMÍG p != NULL és p->key != k
		p = p->next
	HA p != NULL és p->key == k
		return 2
	KÜLÖNBEN
		q = new E2
		HA q == NULL
			return 1
		KÜLÖNBEN
			q->key = k
			q->data = a
			q->next = z[i]
			HA z[i] != NULL
				z[i]->prev = q
			z[i] = q
			return 0
```

#### Pontozás - 17p
* E2 a listaelemek típusa - 1p
* Valahogy jelzi, hogy van egy plusz adattag, és mi a neve - 1p
* A * és a -> elemek megfelelő, végig következetes használata - 1p
* Helyes fejléc - 3p
	* Paraméterek száma, típusa - esetleg a kulcs lehet "T", ha jelzi, hogy T-re értelmezett a modulo
	* A tömb mérete esetleg kimaradhat, de a szögletes zárójel nem. Ha meg van adva, akkor m legyen
	* Return típus kirakása - esetleg N még elfogadható az intervallum helyett
* Hash-függvény meghívása - 1p
* Keresés helyesen, segédpointerrel, figyel a NULL-ra, return 2, ha van találat - 3p
* Allokálás csak akkor, ha már biztosan van mit beszúrni (vagy ha mégse, utólag delete-elés) - 1p
* Allokálás sikerességének vizsgálata, return 1, ha nem sikeres - 1p
* Lista elejére szúrás, felesleges sallangok nélkül, nincs memóriaszivárgás - 3p
* prev pointer állítása NULL-checkkel - 1p
* key és data beállítása; return 0 sem maradt le - 1p

### 4.b, Milyen aszimptotikus becslést tud adni a fenti művelet minimális, átlagos és maximális futási idejére? Miért?

* n legyen a hasítótáblába eddig összesen beszúrt elemek száma
* mT<sub>ins</sub>(n) eleme Θ(1)
	* A modulo hívás konstans
	* Tegyük fel, hogy rögtön a modulo által meghatározott lista első eleme k, ekkor a keresés is konstans és ki is lépünk utána
* AT<sub>ins</sub>(n) eleme Θ(n/m)
	* Várhatóan az elemek egyenletesen vannak elosztva a slotok között, tehát a konstans modulo hívás után egy n/m hosszú listát kell bejárni
	* Várhatóan a lista feléig kell elmenni a kereséssel, ami nagyságrendlieg n/m
	* Ha volt találat, kilépünk; ha nem volt, a lista elejére szúrás konstans műveletigényű
* MT<sub>ins</sub>(n) eleme Θ(n)
	* Tegyük fel, hogy minden eddigi elem hash-kódja azonos, és ráadásul k-t szúrtuk be először, így az n elemű (egyetlen nem üres) lista végén van
	* Ekkor a keresés végén kilépünk, de annak lépésszáma n

#### Pontozás - 8p
* 1-1 pont minden eltalált értékért - 3p
* m indoklása - 1p
* A indoklása - 2p
* M indoklása - 2p
