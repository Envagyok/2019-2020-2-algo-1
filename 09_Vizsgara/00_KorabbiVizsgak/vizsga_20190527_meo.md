# 1. délutáni Algoritmusok és adatszerkezetek I. vizsga - 2019.05.27.
## Részleges megoldás

* A megoldás a saját kidolgozásom, ami főképp az előadásjegyzet nyomán készült
* Néhány esetben - pl. a bucketSort algoritmusa - más jelöléseket is elfogadtam, sőt akár láncolt listás megvalósítást is, ha amúgy az legalább annyira helyes, mint a "referencia" megoldásé
* A bucket sort egy elég nem egyértelmű megfogalmazás, az a) feladatnál, ahol le kellett játszani, többen a Radix Vissza algoritmust játszották le. Ezt - ha hibátlan volt - szintén elfogadtam, hiszen a Radix egy edényrendezés

### 1. feladat

#### 6p - a) Mutassa be a bucket sort működését

* Input: <0,4; 0,82; 0,0; 0,53; 0,73; 0,023; 0,64; 0,7; 0,39; 0,203>
* Kulcstartomány: [0; 1)

* Az input mérete 10, azaz 10 edényünk lesz 0-tól 9-ig, a kulcsokat pedig alsóegészrész(kulcs*10)-zel tudjuk erre leképezni.

* Inicializálás: z = < <>; <>; <>; <>; <>; <>; <>; <>; <>; <> >
* Input feldolgozása egyesével: z = < <0,0; 0,023>; <>; <0,203>; <0,39>; <0,4>; <0,53>; <0,64>; <0,73; 0,7>; <0,82>; <> >
* z elemeinek rendezése: z = < <0,0; 0,023>; <>; <0,203>; <0,39>; <0,4>; <0,53>; <0,64>; <0,7; 0,73>; <0,82>; <> >
* Összefűzés l-be: l = < 0,0; 0,023; 0,203; 0,39; 0,4; 0,53; 0,64; 0,7; 0,73; 0,82 >

#### 13p - b) Struktogram

```
bucketSort(l : List)

z : List of l.size
FOR i = 0 to z.size-1
	z[j] = <>
WHILE !l.isEmpty()
	e = l.removeFirst()
	z[ alsóegészrész(z.size * e.key) ].insert(e)
FOR i = 0 to z.size-1
	z[i].sort()
FOR i = 0 to z.size-1
	l.append(z[i])
```

#### 6p - c) Mekkora aszimptotikusan mT(n), AT(n), MT(n), és milyen feltételekkel?

* Ha az input mérete n, az edények száma is n lesz.
* Ezen az inputon kell végigmenni néhányszor. Ezek minden elemre:
    * z inicializálása - konstans
    * input feldolgozása - lista elejére, vagy végelemesen beszúrást feltételezve konstans
    * z-k rendezése - z méretéhez képest akár négyzetes is lehet, de ez is csak akkor érdekes, ha z mérete nagy
    * összefűzés - lista végéhez fűzni a következő elejét: konstans
* Ha a kulcsokat egyenletesen képezzük le, akkor nagyjából egyeleműek lesznek a listák
    * Innen mT(n) eleme Θ(n)
* Ez általában is teljesül: AT(n) eleme Θ(n)
* MT(n) pedig akkor fordulhat elő, ha nem egyenletes a beszúrás, ekkor egy edénybe kerülhet akár minden és azt kell tovább rendeznünk:
    * Ha négyzetes rendezést használunk (egy edénybe kerül minden): Θ(n<sup>2</sup>)
    * Ha logaritmikus rendezést, akkor Θ(n*log(n))

### 4. feladat

* A Z[0..(m−1)] hasító táblában a kulcsütközést nyílt címzéssel oldjuk fel.

#### 6p - 4.a, Mit értünk kitöltöttségi hányados, próbasorozat és egyenletes hasítás alatt? Mit tudunk a keresés és a beszúrás során a próbák várható számáról?

* Kitöltöttségi hányados: α-val jelöljük, a kitöltött (nem D és nem E) slotok és az összes slot aránya
* Próbasorozat: a tábla [0..m-1] indextartományának egy tetszőleges permutációjának prefixe - ezeket az indexeket látogatjuk sorra a keresés során
* Egyenletes hasítás: ha egy tetszőlegesen kiválasztott potenciális próbasorozat ugyanolyan valószínűséggel adja vissza mind az m! féle permutációt
* Amennyiben nincsenek törölt rések, egyenletes hasítással egy sikertelen keresés / sikeres beszúrás várható hossza (azaz a próbák száma) legfeljebb 1 / (1-α)
* A sikeres keresésé / sikertelen beszúrásé pedig 1/α * ln(1/(1/α))

#### 4p - 4.b, Mekkora egy sikertelen keresés várható hossza 80%-os kitöltöttség esetén, ha nincs törölt rés? Egy sikeres keresésé ennél több vagy kevesebb? Miért?

* Egy sikertelen keresés várható hossza legfeljebb 1 / (1-α), azaz: 1 / 0,2, azaz: 1 * 10/2, azaz: 5
* A sikeresé rövidebb, hiszen a sikertelen attól lesz sikertelen, hogy meggyőződünk róla, hogy a sokadik próbára sincs meg a keresett elem, míg a sikeres esetben jó eséllyel már az első néhány próbára megtaláljuk azt

#### 15p - 4.c, Legyen most m = 11, h1(k) = k mod m, és alkalmazzon lineáris próbát! Az alábbi műveletek mindegyikére adja meg a próbasorozatot <...> alakban!

* Szúrja be a táblába sorban a következő kulcsokat: <10; 22; 31; 4; 15; 28; 16; 26; 62> ezután törölje a 16-ot, majd próbálja megkeresni a 27-et és a 62-t, végül pedig szúrja be a 27-et!
* Szemléltesse a hasító tábla változásait! Rajzolja újra, valahányszor egy művelet egy nemüres rés állapotát változtatja meg!

* Inicializálás:
```
z[E; E; E; E; E; E; E; E; E; E; E]
  0  1  2  3  4  5  6  7  8  9  10
```

* 10 beszúrása - Próbasorozat: <10>
```
z[E; E; E; E; E; E; E; E; E; E; 10]
  0  1  2  3  4  5  6  7  8  9  10
```

* 22 beszúrása - Próbasorozat: <0>
```
z[22; E; E; E; E; E; E; E; E; E; 10]
  0   1  2  3  4  5  6  7  8  9  10
```

* 31 beszúrása - Próbasorozat: <9>
```
z[22; E; E; E; E; E; E; E; E; 31; 10]
  0   1  2  3  4  5  6  7  8  9   10
```

* 4 beszúrása - Próbasorozat: <4>
```
z[22; E; E; E; 4; E; E; E; E; 31; 10]
  0   1  2  3  4  5  6  7  8  9   10
```

* 15 beszúrása - Próbasorozat: <4, 5>
```
z[22; E; E; E; 4; 15; E; E; E; 31; 10]
  0   1  2  3  4  5   6  7  8  9   10
```

* 28 beszúrása - Próbasorozat: <6>
```
z[22; E; E; E; 4; 15; 28; E; E; 31; 10]
  0   1  2  3  4  5   6   7  8  9   10
```

* 16 beszúrása - Próbasorozat: <5, 6, 7>
```
z[22; E; E; E; 4; 15; 28; 16; E; 31; 10]
  0   1  2  3  4  5   6   7   8  9   10
```

* 26 beszúrása - Próbasorozat: <4, 5, 6, 7, 8>
```
z[22; E; E; E; 4; 15; 28; 16; 26; 31; 10]
  0   1  2  3  4  5   6   7   8   9   10
```

* 62 beszúrása - Próbasorozat: <7, 8, 9, 10, 0, 1>
```
z[22; 62; E; E; 4; 15; 28; 16; 26; 31; 10]
  0   1   2  3  4  5   6   7   8   9   10
```

* 16 törlése - Próbasorozat: <5, 6, 7>
```
z[22; 62; E; E; 4; 15; 28; D; 26; 31; 10]
  0   1   2  3  4  5   6   7  8   9   10
```

* 27 keresése - Próbasorozat: <5, 6, 7, 8, 9, 10, 0, 1, 2> -> hamis
```
z[22; 62; E; E; 4; 15; 28; D; 26; 31; 10]
  0   1   2  3  4  5   6   7  8   9   10
```

* 62 keresése - Próbasorozat: <7, 8, 9, 10, 0, 1> -> igaz
```
z[22; 62; E; E; 4; 15; 28; D; 26; 31; 10]
  0   1   2  3  4  5   6   7  8   9   10
```

* 27 beszúrása - Próbasorozat: <5, 6, 7, 8, 9, 10, 0, 1, 2> -> mivel a 2-es üres, ezért nincs benne 27, az első töröltre (7) beilleszthettük
```
z[22; 62; E; E; 4; 15; 28; 27; 26; 31; 10]
  0   1   2  3  4  5   6   7   8   9   10
```
