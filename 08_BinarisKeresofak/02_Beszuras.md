# Beszúrás keresőfába

* Adjuk meg a sima, kiegyensúlyozatlan bináris keresőfába való beszúrás iteratív algoritmusát, ADT szinten, amennyiben a paraméterünk már egy Node (nem csak a kulcs)
* Megoldás:
	* Maga a Node is ADT szinten egy fa
	* Referencia szerint adjuk át a fát, mert lehetséges hogy üres, ekkor magát az üres fát kell átírnunk a paraméterre
	* Az algoritmus első ciklusa meghatározza azt a szülőt, ami alá kerül majd be a p. Akkor érünk a fa aljára, ha az aktuális már ÜRES. Ekkor a szülő fogja annak a szülőjét tartalmazni
	* A szülő akkor és csak akkor ÜRES, ha a fa eleve ÜRES volt, mivel a szülő kezdetben ÜRES, az aktuális pedig a t, és csak akkor lépünk be (és maradunk benn) a ciklusba, amíg az aktuális nem ÜRES. És mivel a szülő mindig az aktuális előző értékét kapja, biztosan nem kaphat ÜRES értéket, csak akkor lehet az, ha sose kapott értéket az inicializálás óta
	* A másik speciális eset, ha a p kulcsa már szerepel a fában. Ekkor nem csinálunk semmit, hiszen kereső- és nem rendezőfa - hibát is jelezhetnénk akár
	* Láncolt ábrázolásnál ezek Node3*-ok lennének a szülő elérhetősége miatt
	
	```
	insert(&t : BinTree, p : BinTree)
	  parent := ÜRES
	  current := t
	  AMÍG current != ÜRES és current.key() != p.key()
	    parent := current
	    HA p.key() < current.key()
	      current := current.left()
	    KÜLÖNBEN
	      current := current.right()
	  HA current != ÜRES és current.key() = p.key()
	    SKIP // már benne van
	  KÜLÖNBEN
	    HA parent = ÜRES // üres fa
	      t := p
	    KÜLÖNBEN
	      p.parent() := parent
	      HA p.key() < parent.key()
	        parent.left() := p
	      KÜLÖNBEN
	        parent.right() := p
	```

* Itt most feltételezzük, hogy lekérhetjük a parent referenciát - de az algoritmus enélkül is megírható - ekkor persze a parentet (mármint nem a parent változót, hanem a p parentjét) érintő értékadás kimarad
