# Θ, Ο, Ω kiszámolása "kézzel"

* Hogy lehet kitalálni mi két műveletigényt leíró függvény közötti aszimptotikus viszony?
    * 1. Definíció szerint
		* Keresünk megfelelő n<sub>0</sub> és c konstansokat
	* 2. Határértékekkel
		* Ha n változó szerint a végtelenben vett lim (f/g) létezik és
			* 0 - akkor f eleme Ο(g) és ο(g)
			* c>0 n-től nem függő konstans - akkor f eleme Ο(g), Ω(g), Θ(g)
			* végtelen - akkor f eleme Ω(g) és ω(g)
		* Használható a L'Hospital szabály (ha f és g is deriválható az adott pontban, akkor a hányados határértéke a deriváltak hányadosának határértéke)
    * 3. Tételek használatával, osztályokba sorolással - erről később

## Konkrét példa

* Bizonyítsuk be definícióból, hogy 2n-1 eleme Θ(n)
	* Egyrészt, keressünk olyan c<sub>1</sub>-et, hogy: c<sub>1</sub>*n <= 2n-1
		* Osszunk le n-nel: c<sub>1</sub> <= 2 – 1/n. A jobb oldal második tagja ahogy n nő, elmegy 0-ba, n=1-re még pont 1, utána egyre csak csökken, tehát maga a különbség nő, 2-be tart
		* Ezek után kimondhatjuk, ha n legalább 1 (hogy oszthassunk vele), ideális választás c<sub>1</sub>-nek bármi ami kisebb vagy egyenlő, mint 1, hiszen még a minimális esetben is csak annyi lesz
	* Másrészt, kell olyan c<sub>2</sub>, hogy: 2n-1 <= c<sub>2</sub>*n
		* Hasonlóan, osztunk: 2 - 1/n <= c<sub>2</sub>. Most is, ahogy n-t növeljük, a kifejezés 2 felé tart, de alapvetően ezt sosem éri el
		* Nekünk olyan c<sub>2</sub> kéne, ami nagyobb-egyenlő annál ami épp hogy mindig kisebb, mint 2, tehát c<sub>2</sub>>=2-nek kell igaznak lennie
* Tehát: n<sub>0</sub>>=1, c<sub>1</sub><=1, c<sub>2</sub>>=2
* Így pl. n<sub>0</sub>=1, c<sub>1</sub>=1, c<sub>2</sub>=2 helyes választás
