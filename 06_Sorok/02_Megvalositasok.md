# Sor

## Ábrázolások

### Tömbös

* Lehetne - a/1 : T[n]; - e : [0..n] (end - vége)
    * Ekkor a[1]-re szúrunk be először, majd így tovább, mindig az e változó adja meg az utolsó beszúrt elemet (ez kezdetben 0, mivel üres a sor)
    * Viszont így minden kivételkor shiftelni kéne balra az egész tömböt, így ez a művelet nem lenne konstans műveletigényű
* Lehetne akkor - a/1 : T[n]; - s : [1..n] (start - eleje); - e : [1..n], de ilyenkor kellene egy plusz adattag, mert ha e = s-1, azzal az üres sort éppúgy lehetne reprezentálni, mint a tele sort - bár ez 0-val indexeléssel is megoldható probléma
* Mi végül is ezt a reprezentációt választottuk:
	* \- a/1 : T[n]
	* \- s : [1..n]
	* \- l : [0..n] (length - hossza)
* Néhány metódus implementációja

	```
	Queue::Queue()
	  s := 1
	  l := 0
	```

	```
	Queue::add(x : T)
	  HA l = n
		HIBA
	  KÜLÖNBEN
		HA s + l <= n
		  a[s+l] := x
		KÜLÖNBEN
		  a[s+l-n] := x
		l := l+1
	```

	* A végére rakok, kivéve ha túlcsordulna, mert akkor az elejétől folytatom, amíg ott van hely
	* Kitérő: a gyakorlatban a tömbös reprezentációk gyakran olyanok, hogy a kapacitás betelése esetén nem hibajelzéssel térnek vissza, hanem megnövelik a kapacitást (ami egy teljesen természetes dolog, hiszen a kapacitás (n) nem is látható kívülről és az interfész alapján nem is szabadna lennie ilyennek). Ezt egyéb más módok mellett általában egy nagyobb mögöttes tömb újraallokálásával és az eddigi tömb egyesével való átmásolásával lehet elérni, ami azt eredményezi, hogy az amúgy konstans műveletigényű műveletek néha lineárisak lesznek, de ez nem az inputtól, hanem az adatszerkezet pillanatnyi belső állapotától függ. Ezt hívjuk "amortizált konstans" műveletigénynek

	```
	Queue::rem(&x : T)
		HA l = 0
			HIBA
		KÜLÖNBEN
			x := a[s]
			l := l-1
			HA s = n
				s := 1
			KÜLÖNBEN
				s := s+1
	```

	* Az előző inverze. Értelemszerűen az l csökken, de ez nem elég, mert akkor a "végéről" vennénk el elemet, ahhoz hogy az első essen ki, a sor eleje változót növelni kell (modulóban értve)

### Láncolt implementáció		

* \- s eleme E1* (start)
* \- e eleme E1* (end)
	* És mindezt úgy, hogy van egy a fejelemhez hasonló helyőrző, amire kezdetben mutat mindkét pointer, majd utána mindig az e fog csak erre: ez lesz annak a helye, ahova majd beszúrok, hivatalosan ez nem a lista része. Ezt hívjuk a fejelem mintájára végelemnek
* Műveletek:

	```
	Queue::Queue()
		e := new E1
		s := e
	```

	```
	Queue::add(x : T)
		e->key := x
		e->next := new E1
		e := e->next
	```
	
	* Kitöltöm a már talonban levő e-t, beszúrok mögé egy új e-t, aminek a mutatója helyesen NULL-ra inicializálódik, mint ahogy egy tisztességes végelemhez illik (analógia: a kétirányú fejelemes listáknál is NULL volt a fejelem visszapointere)

	```
	Queue::rem() : T
		HA s = e
			HIBA
		KÜLÖNBEN
			x := s->key
			p := s
			s := s->next
			delete p
			return x
	```
	
	* A hiba esete az üres lista; itt: csak ha végelem van, azaz az a szituáció, amit a konstruktor hozott össze
	* Különben ez a lista elejéről való törlés minősített esete
	* Technikai okokból a kulcsot és a start pointert is ki kell rakni segédváltozókba. Előbbit, hogy vissza tudjunk térni vele az elem törlése után is; utóbbit pedig hogy a startot át tudjuk mutattatni, mielőtt töröljük az ehhez szükséges információt
* Opcionálisan a láncolt reprezentáció is kiegészíthető egy hossz attribútummal, és a vele visszatérő length() függvénnyel
	* Lehet nem végelemes listával is megadni, ekkor s és e is lehet NULL