# Negyedik zh

## Ihlet korábbi félévek zh-iból

* A 00_Feladatok/00_KorabbiFelevek mappába feltöltött elemek közül az alábbiak az alábbi megkötésekkel vonatkoznak az idei negyedik zh tematikájára

### 2017.12.14. - második zh

* Fájlok:
    * zh2_20171214_fa.pdf
    * zh2_20171214_meo.pdf
    * zh2_20171214_meo_JAV.md - a teljesség kedvéért, de egy olyan feladat megoldásának javítása szerepel benne, ami nem kell a zh-hoz
* Vonatkozó feladat:
    * 1/a.
    * 1/b. a kupacra vonatkozó része nélkül
* Megjegyzések:
    * A 3. feladatról szóló anyagot a "pluszanyagok" között látjátok, érdemes átnézni ezt is, mert vizsgaanyag, de a zh-hoz nem kell
    * A 2. feladat azóta már az Algo 2. tananyagának része
    * BinKerFa::beszúr ~ BinTree::insert
    * p típusa nincs kitéve, ebben a félévben ez volt a rendszer, persze ez BinTree típusú
    * Omega ~ üres fa
    * kulcs(p) ~ p.key(), szülő, bal, jobb hasonlóan
    * 1/b-t is érdemes értően átolvasni, szoktunk feladni zh-ban is és vizsgán is ilyen kvázi gondolkodósabb és/vagy beugratósabb kérdéseket
    * A kupacról ezen a ponton érdemes tudni, hogy egy rendezéshez használt, de nem rendezőfa, amit tömbösen ábrázolunk, az elemei úgy vannak elrendezve, hogy minden csúcsnak kisebb kulcsúak a gyerekei, mint önmaga, illetve a tömbös ábrázolásában nincsen "lyuk", balról jobbra van feltöltve
    
### 2018.12.14. - második zh

* Fájlok:
    * zh2_20181214_fa.pdf
    * zh2_20181214_meo_1.jpg
    * zh2_20181214_meo_2.jpg
    * zh2_20181214_meo_3.jpg
* Vonatkozó feladatok:
    * 1.
    * 3.
* Megjegyzések:
    * A 2. feladatot is érdemes átolvasni, hiszen a kupac is bináris fa
    * A kupacra vonatkozó alapismereteket lásd fentebb
    * A 3-asnál az inorder bejárás ismerete volt a kulcskérdés
    * key(t) ~ t.key(), stb.
    * A 2. feladatban a "B" a bool típust jelenti (idén "L"), a tömb kicsit másképp van deklarálva és 1-től indexelve, illetve az értékadás csak egy egyenlőségjel

### 2019.05.15. - második zh

* Fájlok:
    * zh2_20190515_fa.pdf
    * zh2_20190515_meo.pdf
* Vonatkozó feladatok:
    * 1/b.
    * 2.
    * 3.
* Megjegyzések:
    * Az 1-eshez azért az a-t is érdemes átfutni, hogy jobban kontextusba kerülj, mi is a feladat
    * Az 1-esnél az értékadás a "=", az egyenlőségvizsgálat a "=="
    * A 3-as feladatban definiált adatszerkezetet speciel a 2. félévben fogjuk tanulni (kicsit msáképp), a tavalyiaknak ugyanúgy anélkül volt ez a feladat, hogy tanulták volna Algo1-ből
    * A stringeket itt 1-től indexeltük, idén defaultból 0-tól