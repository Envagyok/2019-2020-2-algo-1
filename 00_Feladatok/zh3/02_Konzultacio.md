# Algoritmusok és adatszerkezetek 1., harmadik zh konzultáció

* Helye: MS Teams
* Ideje: 2020. 05. 05. kedd, 18.00 – 18.50

## 1. feladat - Rendezés két veremmel

* Adott egy veremben néhány szám, ürítsük ki ezt a vermet, és egy másik verembe helyezzük át a számokat, de úgy, hogy rendezett sorrendben legyenek. A két vermen kívül más adatszerkezetet nem használhatunk, a szokásos verem interfész műveletei állnak csak rendelkezésünkre
* Megoldás:
	* Csak a lejátszást adtuk meg, a konkrét struktogram **házi feladat** volt
	* A vermet így fogom jelölni: [a, b, c. Ez azt jelenti, hogy a veremtető "c", alatta "b", és az alatt "a" van a veremben
	* Legyen a példabemenet az alábbi:
		* input = [25, 2, 12, 30, 4
	* Hozzunk létre egy újabb üres vermet, legyen a neve output, majd ezzel fogunk visszatérni
	* Vegyük le az input felső elemét (csak ehhez férünk hozzá eleve), majd helyezzük át:
		* input = [25, 2, 12, 30
		* output = [4
	* Ezt ismételgessük egészen addig, amíg vagy ki nem ürül az input (ekkor kész is vagyunk), vagy nem lesz igaz az az állítás, hogy van az outputnak eleme és ez a felső elem nagyobb, mint az input felső eleme:
		* input [25, 2, 12
		* output [4, 30
	* Azaz itt meg is állunk, a 30 nagyobb, mint a 12
	* Ilyenkor a 12-t kirakjuk egy ideiglenes változóba, és egészen addig, amíg létezik az output teteje és nagyobb, mint a kivett elem, visszarakosgatjuk az outputról az inputra az elemeket:
		* input [25, 2, 30 temp = 12
		* output [4
	* Amikor vagy kifogyott az output, vagy már nem nagyobb a teteje 12-nél, berakjuk:
		* input [25, 2, 30
		* output [4, 12
	* És folytatjuk az eddigi eljárást, amíg az input ki nem ürül:
		* input [25, 2
		* output [4, 12, 30
	* Most nagyobb az output teteje:
		* input [25, 30, 12, 4 temp = 2
		* output [
	* Kiürült, ezért berakhatjuk a 2-t:
		* input [25, 30, 12, 4
		* output [2
	* Most megint átrajuk a nagyobb elemeket:
		* input [25
		* output [2, 4, 12, 30
	* Most a 25 jönne, ami kisebb, mint a veremtető, ezért félretesszük, és az összes nagyobbat (egyedül a 30) visszarakjuk az inbe:
		* input [30 temp = 25
		* output [2, 4, 12
	* Mehet a 25, majd a 30 az outba:
		* input [
		* output [2, 4, 12, 25, 30
	* Kész

## 2. feladat - Palindrom 2 veremmel

* Döntsük el egy "stringstream" bemenetről (nem ismert előre a hossza), hogy egy palindrom szót ad-e. Használjunk verme(ke)t!
* Megoldás:
	* Először is nézzük meg, mi lenne, ha a bemenet tömbszerű lenne, azaz indexelhető és lekérdezhető méretű:
		
		```
		iPalindrome(s : String) : L
			i := 0 to alsóEgészrész(|s|/2)-1
				HA s_i != s_|s|-i+1
					return false
			return true
		```	
		
		* Páratlan méretű stringnél a középső elem önmagával kellene hogy egyenlő legyen, ezért ezt külön nem is vizsgáljuk
	* Vermes megoldásnál ha véletlen tudnánk az input méretét, akkor a következőt tennénk:
		* Beolvassuk a feléig a karaktereket egy verembe
		* Ha páratlan számú, eldobjuk a középsőt
		* És most, az inputon maradó második felét egyesével olvassuk be, minden soron következő elemet összehasonlítunk a veremtetővel (amit utána el is távolítunk), itt ugyebár a veremből pont fordított sorrendben fogjuk elővarázsolni a string első felét, ami épp megfelel szándékainknak
	* Ha nem ismerjük a méretet, akkor előbb egy verembe beolvassuk karakterenként, ezzel együtt számoljuk a beolvasott karaktereket, majd a fenti logika alapján a felét átmásoljuk egy másik verembe, megvizsgáljuk a méret paritását, majd a két verem elemeit sorban összehasonlítjuk:
		
		```
		isPalindrome(in : InStream) : L
			s : Stack
			n := 0
			ch := in.read()
			AMÍG ch != EOF
				s.push(ch)
				n := n+1
				ch := in.read()
			s2 : Stack
			i := 1 to alsóEgészrész(n/2)
				s2.push(s.pop())
			HA NEM(2 | n)
				s.pop()
			AMÍG !s.isEmpty()
				HA s.pop() != s2.pop()
					return false
			return true
		```

    * Mivel n az input mérete, ezért az input feléig menő pop hívások - amiket nem előzött meg isEmpty hívás a helyességüket ellenőrizendő - szabályosak
    * Hasonlóan a végén, bár csak s nemürességét vizsgáljuk, de értelemszerűen ekkor s2 se lehet üres

## 3. feladat - Névsorfordítás 2 veremmel

* Írjunk algoritmust, ami egy vesszőkkel elválasztott, kettőskereszttel terminált, szavakat tartalmazó szöveget képes ugyanilyen formátumban, de fordított sorrendben visszaadni
* Megoldás:
	* Példa: in := Andras,Bea,Csilla,..,Zeno# -> out := Zeno,...,Csilla,Bea,Andras#
	* Karakterenként olvassuk, nem ismerjük a hosszát
	* A verem kiválóan alkalmas ilyen megfordítós feladatokra, de itt most az a feladat, hogy a vesszők mentén fordítva írjuk ki az elemeket, de a vesszőkön belül ne fordítsuk meg a sorrendet (de úgy is fogalmazhatok, hogy megfordíthatjuk, ha utána visszafordítjuk)
	* Ezért ennél a feladatnál két vermet fogunk használni
	* Két megoldási ötletet is elmondok, ebből az elsőt kidolgozom, a második **házi feladat**
		* Első megoldás: Bedobok mindent s1-be. De ekkor fordítva kerülne bele minden, ezért amikor elválasztó jön, akkor ami eddig volt s1-ben, azt s2-be rakom át, ami által újra megfordul a sorrend. Majd a végén s2-t kiírom
		* Második megoldás: Bedobok mindent s1-be, majd miután kész vagyok ezzel, végigmegyek s1 elemein és átpakolom s2-be, de úgy, hogy amikor elválasztójel jön, akkor mindig kiírok s2-ből (**HF**)

		```
		reverseOrder(in : InStream, &out : OutStream)
			out := <>
			s1 : Stack
			s2 : Stack
			s2.push('#')
			ch := in.read()
			AMÍG ch != '#'
				AMÍG ch != '#' és ch != ','
					s1.push(ch)
					ch := in.read()
				AMÍG !s1.isEmpty()
					s2.push(s1.pop())
				HA ch == ','
					s2.push(',')
					ch := in.read()
			AMÍG !s2.isEmpty()
				out.print(s2.pop())
		```	
	
	* s2-be már a kezdetek kezdetén berakok egy #-et, hogy a végén az legyen az utolsó kiírandó elem
	* Elsőre nem biztos, hogy nyilvánvaló, miért ismételjük meg az AMÍG ch != '#' feltételt a belső ciklusban
		* A külső ciklus a teljes stringet nézi meg, míg azon belül a belső szavanként terminál. Az egyik (konkrétan az utolsó) szó végén viszont a # van, hát ezért
		* A HA ch ==',' feltétel is magyarázatra szorul: azt nézem itt, hogy a belső ciklus ugye nem azért ért véget, mert # volt, hanem mert vessző. Ha ez így van, akkor egyrészt majd be kell raknunk egy vesszőt a kiírandók közé (nyilván az utolsó szó után nem, mert akkor vesszővel kezdődne majd az output), és tovább is kell olvasnunk, hogy a következő szó elejével folytathassuk. Kettőskereszt esetében meg simán a következő körben terminálni fog a külső ciklus magától is
* Ha a tömbös veremimplementációt alkalmazzuk, lehet a két vermet egy és ugyanazon statikus méretű tömb elejeképp és végeképp megadni, mivel a két verem összméretének maximuma konstans

## 4. feladat - Lengyelformára hozás és kiértékelés

* Írjuk fel a lengyelformát egy verem segítségével kiértékelő függvényt, majd a megadott inputot a tanult módszerrel alakítsuk át RPN-re és utána értékeljük is ki a készített algoritmus segítségével! Input: in := ( ( ( 2 + 3 ) ^ 2 / 5 ) ^ 2 - 6 - 4 / 2 - 8 ) + ( 2 + 3 )
Megoldás:
	* A lengyelformát kiértékelő függvény hasonló elven működik, mint amit a teljesen zárójelezett kifejezések kiértékelésénél már megnéztünk - végül is, ugyanazt a feladatot oldjuk meg most is
	* Tehát ha operandust (változó vagy literál) olvasunk, berakjuk egy verembe, ha operátort (művelet), akkor pedig (élve azzal a feltevéssel, hogy csak binér műveleteink vannak) a verem két felső elemét összeműveleteljük majd az eredményt rakjuk be a verembe
	* A végén a végső eredmény fog a verem alján lapítani
	* Figyelem: amikor műveletet végzünk, vegyük figyelembe, hogy a jobb oldali paramétert vesszük ki előbb, tehát épp fordítva kell a paramétereket átadni a műveletnek, mint ahogy jöttek
	
		```
		evalRpn(in : InStream) : Q
			s : Stack
			ch := in.read()
			AMÍG ch != EOF
				HA ch eleme OPERAND
					s.push(ch)
				KÜLÖNBEN
					right := s.pop()
					s.push(eval(s.pop() KONKAT ch KONKAT right))
				ch := in.read()
			return s.pop()
		```
		
	* Most az órán megismert módszerrel hozzuk lengyelformára ezt a kifejezést:
		* in := ( ( ( 2 + 3 ) ^ 2 / 5 ) ^ 2 - 6 - 4 / 2 - 8 ) + ( 2 + 3 )
	* Karakterenként olvasunk, minden körben kiírom az output és a verem állapotát
		* s - operandus, kiírjuk
			* s = [
			* out = s
		* := - operátor, berakjuk
			* s = [:=
			* out = s
		* ( - nyitó zárójel, berakjuk
			* s = [:= (
			* out = s
		* ( - nyitó zárójel, berakjuk
			* s = [:= ( (
			* out = s
		* ( - nyitó zárójel, berakjuk
			* s = [:= ( ( (
			* out = s
		* 2 - operandus, kiírjuk
			* s = [:= ( ( (
			* out = s 2
		* plusz - operátor, berakjuk
			* s = [:= ( ( ( +
			* out = s 2
		* 3 - operandus, kiírjuk
			* s = [:= ( ( ( +
			* out = s 2 3
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* s = [:= ( (
			* out = s 2 3 +
		* ^ - operátor, berakjuk
			* s = [:= ( ( ^
			* out = s 2 3 +
		* 2 - operandus, kiírjuk
			* s = [:= ( ( ^
			* out = s 2 3 + 2
		* / - operátor, kiírjuk a nagyobb precedenciájúakat, majd berakjuk
			* s = [:= ( ( /
			* out = s 2 3 + 2 ^
		* 5 - operandus, kiírjuk
			* s = [:= ( ( /
			* out = s 2 3 + 2 ^ 5
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* s = [:= (
			* out = s 2 3 + 2 ^ 5 /
		* ^ - operátor, berakjuk
			* s = [:= ( ^
			* out = s 2 3 + 2 ^ 5 /
		* 2 - operandus, kiírjuk
			* s = [:= ( ^
			* out = s 2 3 + 2 ^ 5 / 2
		* mínusz - operátor, kiírjuk a nagyobb precedenciájúakat, majd berakjuk
			* s = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^
		* 6 - operandus, kiírjuk
			* s = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6
		* mínusz - operátor, kiírjuk az egyenlő precedenciájúakat a balasszociativitás miatt, majd berakjuk
			* s = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 -
		* 4 - operandus, kiírjuk
			* s = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4
		* / - operátor, berakjuk
			* s = [:= ( - /
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4
		* 2 - operandus, kiírjuk
			* s = [:= ( - /
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2
		* mínusz - operátor, kiírjuk a nagyobb-egyenlő precedenciájúakat, majd berakjuk	
			* s = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / -
		* 8 - operandus, kiírjuk
			* s = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* s = [:=
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 -
		* plusz - operátor, berakjuk
			* s = [:= +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 -
		* ( - nyitó zárójel, berakjuk
			* s = [:= + (
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 -
		* 2 - operandus, kiírjuk
			* s = [:= + (
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2
		* plusz - operátor, berakjuk
			* s = [:= + ( +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2
	    * 3 - operandus, kiírjuk
			* s = [:= + ( +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* s = [:= +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3 +
		* Vége az inputnak, kiírok mindent a veremből
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3 + + :=
	* Most pedig futtassuk le a feladat elején definiált függvényt:
	* Az előző rész outputja az input, karakterenként tudjuk olvasni: < s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3 + + := >
		* s - operandus, berakjuk
			* s = [s
		* 2 - operandus, berakjuk
			* s = [s 2
		* 3 - operandus, berakjuk
			* s = [s 2 3
		* plusz - operátor, kivesszük, kiértékeljük a 2+3-at, berakjuk
			* s = [s 5
		* 2 - operandus, berakjuk
			* s = [s 5 2
		* ^ - operátor, kivesszük, kiértékeljük a 5^2-t, berakjuk
			* s = [s 25
		* 5 - operandus, berakjuk
			* s = [s 25 5
		* / - operátor, kivesszük, kiértékeljük a 25/5-öt, berakjuk
			* s = [s 5
		* 2 - operandus, berakjuk
			* s = [s 5 2
		* ^ - operátor, kivesszük, kiértékeljük a 5^2-t, berakjuk
			* s = [s 25
		* 6 - operandus, berakjuk
			* s = [s 25 6	
		* mínusz - operátor, kivesszük, kiértékeljük a 25-6-ot, berakjuk
			* s = [s 19
		* 4 - operandus, berakjuk
			* s = [s 19 4
		* 2 - operandus, berakjuk
			* s = [s 19 4 2
		* / - operátor, kivesszük, kiértékeljük a 4/2-t, berakjuk
			* s = [s 19 2
		* mínusz - operátor, kivesszük, kiértékeljük a 19-2-t, berakjuk
			* s = [s 17
		* 8 - operandus, berakjuk
			* s = [s 17 8		
		* mínusz - operátor, kivesszük, kiértékeljük a 17-8-at, berakjuk
			* s = [s 9
		* 2 - operandus, berakjuk
			* s = [s 9 2
		* 3 - operandus, berakjuk
			* s = [s 9 2 3
		* plusz - operátor, kivesszük, kiértékeljük a 2+3-at, berakjuk
			* s = [s 9 5
		* plusz - operátor, kivesszük, kiértékeljük a 9+5-öt, berakjuk
			* s = [s 14
		* := - operátor, kivesszük, kiértékeljük az s := 14-et, berakjuk
			* s = [14
		* S ezzel térünk vissza (persze mellékhatásként az s változó értéke is 14 lett)
	* Érdemes ilyenkor egyszer kiszámolni az infix változatot is fejben, hogy meggyőződjünk, jól csináltuk
