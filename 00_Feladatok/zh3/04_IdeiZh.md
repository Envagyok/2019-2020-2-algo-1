# 2020-as harmadik zh

* A zh-t online (canvas) írtuk 2020. 05. 12-én 16.00 és 16.35 között 30 perces időkerettel
    * Alapvetően két feladat volt
        * (1) Mindenki kapott 3 db állítást, amiről el kellett dönteni, az igaz-e. Minden állítás 1 pontot ért. Ezen túl ezeket röviden indokolni is kellett (ha ez nem volt egyértelmű, a konkrét feladatnál szerepelt, hogyan). Az indoklás 2-2 pontot ért állításonként, részpontok adhatók voltak. ÖSszesen tehát ebből 9 pont tudott kijönni
        * (2) Egy kódolós feladat 11 pontért
* A zh tehát 20 pontos volt
* A második feladatnál alapvetően szöveges megoldásokat vártam, de Teamsen el lehetett a zh vége utáni 3 percben még fényképesen, ha valaki rajzolva akarta megoldani
* A feladatokat random osztotta a rendszer, az alábbiakban feladatcsoportonként megadom az összes lehetséges feladatot és azok megoldásait

## Első feladatcsoport - Igaz-hamis indoklásokkal

* Nem lehet a hatékonysággal kapcsolatos elvárások megtartásával egy olyan adatszerkezetet készíteni, ami sor és verem is egyben. Tehát a végére lehet add()-dal és push()-sal is pakolni, lehet belőle rem()-mel és pop()-pal is kivenni. Persze a FIFO és LIFO tulajdonság sérülhet, ezt engedjük. Ha tényleg nem lehet, írd meg miért: mert nem lehet "fizikailag", vagy mert nem hatékony; ha lehet, akkor röviden írd le, hogy nézne ki egy ilyen implementáció
    * Hamis
        * Ugyanis lehet. Legyen kétirányú lista a reprezentáció. A rem() az első elemet veszi el és lépteti tovább. A push() és add() ugyanaz, az utolsó elem utánra szúr be. A pop() pedig az utolsó elemet veszi ki úgy, hogy az utolsó elemet számon tartó pointert eggyel előrébb lépteti. Ez mind konstans idő
        * Tömbös ábrázolással is: induljunk ki a sor shiftelős eleje-hossz-os reprezentációjából és adjuk meg a lehetőséget a végéről való elvételre is (a length csökkentésével)
* A hagyományos sorra való hatékonysági megkötések mellett lehet kétirányú sort készíteni. Tehát lehet az elejére és a végére is berakni és kivenni onnan, a "FIFO"-ság persze ez esetben nem elvárás. Indoklás: vagy írd le miért nem lehet "fizikailag" vagy legalábbis jó hatékonysággal, vagy ha lehet, akkor néhány mondatban foglald össze, kb. mit változtatnál a tanultak szerinti megvalósításon
    * Igaz
        * Ugyanis ezt is lehet. Megint szerencsés ha kétirányú a lista, esetleg ha ciklikus is, akkor nem is kell két pointer. De ha kétirányú, akkor mindenképp tudunk mindkét végével mindent csinálni konstans időben
        * Tömbös ábrázolással is magától értetődően megoldható
* Megoldható egy tömbben egy verem és egy sor együttes reprezentálása, ha tudjuk, hogy a verem és a sor összesített mérete mindig a tömb mérete alatt marad. Tehát pl. a tömb elején a verem, a végén a sor. A műveletek ugyanúgy megoldhatók konstans műveletigénnyel. Igazoláshoz nem kérem a teljes kidolgozást, csak néhány mondatban győzz meg, hogy ez tényleg így van. Ha hamis az állítás, akkor elég csak az egyik művelet nem megvalósíthatóságát igazolni
    * Igaz
        * Mindkét adatszerkezet reprezentálható tömbösen. Ha úgy vesszük, hogy a tömb első n/2 eleme egy tömb és a második n/2 eleme egy másik tömb, akkor máris kész vagyunk a tömbbel és 4 indexváltozóval
        * De a két adatszerkezet közti határ nem változhat dinamikusan. Alapvetően a verem lehet a tömb elején vagy végén, a sor viszont kialakításából adódóan akár az egész tömböt bejárhatja. Így előfordulhat olyan konstelláció, hogy a tömb első néhány indexén a verem adatai vannak, aztán a sor, aztán a verem, stb.
        * Viszont ez mit is jelent a gyakorlatban? Minden elemről külön számon kell tartani az ő rákövetkezőjét (esetleg megelőzőjét). Ez pedig nem más, mint a tömbös láncolt lista repreztentáció. Egy tömbön belül két ilyen listát tudunk tárolni, és mindkét lista esetében az elejére és a végére szúrás is konstans
* Lehet lengyelformát verem nélkül kiértékelni (nem bináris fával, mert az még nem ennek a zh-nak az anyaga). Indoklás: ha igaz, írd le egy-két mondatban a használt segédstruktúrá(ka)t, ha van ilyen, ill. az algoritmus logikáját. Ha nem, érvelj, miért lehetetlen küldetés ez!
    * Igaz
        * Nyilván lehet többszörös végigolvasással is, de mégegyszerűbb a megoldás. Vegyünk egy láncolt listát, amit használjunk veremként. Mivel a vermet lehet láncolt listával reprezentálni, ezért ez működik. Ez a lista többet is tud ugyan, de mi csak veremként használjuk. Effektíve vermet nem használtunk, az algoritmus mégis ugyanaz
* Egy lengyelformából egyértelműen kiderül, hogy mi volt az ő infix formájának fő műveleti jele
    * Igaz
        * A lengyelforma nyilván megfeleltethető az infix formának. Az infix forma fő műveleti jele az, amit legutoljára végeznénk el (ez teljes zárójelezésnél könnyen látszik, zárójelek nélkül pedig az asszociációk és precedenciák alapján található ki). Lengyelformában mindig az utolsónak leírt műveleti jel az amit utoljára végzünk el, tehát az utolsónak leírt karakter lesz a fő műveleti jel
* Egy infix kifejezésnek több RPN megfelelője is lehet
    * Hamis
        * Mind az infix, mind a postfix kifejezést egyértelmű módon lehet kiértékelni. Előbbinél a precedenciák, asszociativitások ismerete miatt, utóbbi esetében viszont tisztán az alakja miatt. Épp emiatt - mivel zárójelek sincsenek - nincs variációs lehetőség, egy bizonyos kifejezésnek egy bizonyos RPN alakja lehet
        * Egy RPN-hez már tartozhat több infix alak: a zárójelek eltérhetnek
        * Az is lehet persze, hogy két különböző RPN-nek ugyanaz lesz az eredménye, de az már nem formai kérdés
* Az "ab+c+d", "aaa++" és "a+b+c" kifejezések egyike se lehet RPN. Indoklás: mindhez egyesével támaszd alá miért igen, miért nem!
    * Hamis
        * Nézzük sorra
        * "ab+c+d"
            * Alapvetően az operátorok és operandusok száma nem változik, tehát utóbbiaknak eggyel többen kell lennie. Ez itt nem teljesül. Az eleje "a+b" infixül, és ehhez még hozzáadjuk c-t, de a d-hez nincs művelet
            * RPN végén csak operátor lehet
        * "aaa++"
            * Előbb "a+a" majd még ehhez a, tehát ehhez az "a+a+a" egy jó infix alak. Mivel ez valid, emiatt hamis az állítás
        * "a+b+c"
            * Az első két karakternek operandusnak kell lennie, ha már binér minden operátor, nem tudjuk mi az első plusz jel második paramétere
* Képzeljünk el saját binér műveletkészletet egyedi precedenciákkal és asszociativitási szabályokkal. Zárójelek is lehetnek. Azt állítom, egy zárójeleket **nem** tartalmazó RPN **is** kevesebb biten adható át, mint egy azonos információtartalmú infix megfelelője. Igaz-e ez? Miért?
    * Igaz
        * Ugyanis, ha saját műveletekről van szó, akkor ezeket valahogy meg kell adni: mi a jele, mi az asszociativitása, mi a precedenciája. Ezt pedig át kell adni. RPN-nél ez mind nem releváns infó
* Azt tanultuk, hogy összefuttatásnál lehet az inputként megadott egyik vagy akár mindkét Queue-t kvázi "immutable"-ként kezelni. Lehetséges-e ez akkor is, ha vermeket futtatunk össze? Ha igen, hogyan? Ha nem, miért nem?
    * Igaz
        * Hogy is értük ezt el? Beraktunk egy extremális elemet, mindig amikor olvastunk, azt vissza is írtuk, majd a végén kivettük az extremális elemet
        * Veremmel ez nem teljesen működik, mert a végén az eredeti elemek fordított sorrendben lesznek majd a veremben, de segédveremmel megoldható. Akár a program végén másoljuk át az eredetibe a segédből, akár menet közben


## Második feladatcsoport - Vermes-soros kódolós/típusimplementálós feladatok

### Első variáció

* Készíts láncolt veremimplementációt
* Használj kétszeresen láncolt listát
* Add meg a push(x : T), pop(&x : T), top() : T műveleteket
* A verem működjön úgy, hogy ha többször egymás után ugyanazt az elemet rakják be, csak egyszer mentse el
* Tehát a s.push(5) hatása legyen ugyanaz, mint az s.push(5); s.push(5) hatása, de nem ugyanaz, mint az s.push(5); s.push(3); s.push(5) hatása
* Egyéb szempontból működjön úgy, mint egy hagyományos verem

#### Megoldás

* A megoldás része a reprezentáció is. Minden adattagra: változónévvel, típussal, láthatósággal
* Reprezentáció:

    ```
    - sp : E2*
    ```

* Hiszen kétirányú kell legyen
* Metódusimplementációk:

	```
	Stack::Stack()
	    sp := NULL
	```

* A kétszeresen láncolt üres lista is NULL pointer

	```
	Stack::push(x : T)
	    HA sp == NULL V sp->key != x
	        p := new E2
	        p->key := x
	        p->next := sp
	        HA sp != NULL
	            sp->prev := p
	        sp := p
	```

* Csak akkor szúrunk be, ha még vagy nem volt elem, vagy a legutóbbi nem épp x
* p prevje alapértelmezés szerint NULL

	```
	Stack::pop(&x : T)
	    HA sp == NULL
	        HIBA
        KÜLÖNBEN
            x := sp->key
            HA sp->next != NULL
                sp->next->prev := NULL
            t := sp
            sp := sp->next
            delete t
	```

* Arra kell figyelni, x-et referencia szerint adtuk át, nem pedig return értéknek szántuk
* Figyeljünk az értékadások sorrendjére
* A next prevjét csak akkor frissítsük, ha létezik a next

    ```
    Stack::top() : T
        HA sp == NULL
            HIBA
        KÜLÖNBEN
            return sp->key
    ```

### Második variáció

* Készíts láncolt sor implementációt fejelemes és egyben végelemes listával
* Add meg a add(x : T), a rem(&x : T) és a first() : T metódusok implementációját
* A sor működjön úgy, hogy ha többször egymás után ugyanazt az elemet rakják be, csak egyszer mentse el
* Tehát a q.add(5) hatása legyen ugyanaz, mint a q.add(5); q.add(5) hatása, de nem ugyanaz, mint a q.add(5); q.add(3); q.add(5) hatása
* Egyéb szempontból működjön úgy, mint egy hagyományos sor

#### Megoldás

* Reprezentáció:

    ```
    - head : E1*
    - last : E1*
    - end : E1*
	```

* Metódusimplementációk:

	```
	Queue::Queue()
	    head := new E1
	    last := head
	    end := new E1
	    head->next := end
	```
* Tehát amikor üres, akkor van egy fejeleme, egy végeleme, a fejelem a végelem előtt van, és majd a kettő között lesz a sor érdemi része
* A lasttal az utolsó érdemi elemet (vagy ha nincs ilyen, a fejelemet) jelöljük
* Ez az ismételt beszúrás ellenőrzésére kell
* Alternatív megoldás, és elég két pointer, ha kétirányú is a lista vagy ha a "végelemest" nem úgy fogjuk fel, hogy van egy felesleges elem a végén, hanem a végelem eleve az utolsó érdemi elemre mutat

    ```
    Queue::add(x : T)
        HA last = head V last->key != x
            end->key := T
            end->next := new E1
            last := end
            end := end->next
	```
	
	* A beszúrás elején ellenőrizzük, hogy szabad-e ezt megtennünk, azaz az utolsó elem, ha létezik, nem azonos kulcsú-e a most beszúrandóval
	* Utána ugyanazt tesszük, ahogy a tanult verzióban, csak a lastot is léptetjük
	
    ```
    Queue::rem(&x : T)
        HA head = last
            HIBA
        KÜLÖNBEN
            x := head->next->key
            t := head->next
            HA last = t
                last := head
            head->next := t->next
            delete t
	```

    * A feltétel az üres sor esete
    * Viszonylag egyértelmű a kód, két dologra kell figyelni:
        * Itt nem returnölünk, hanem referencia szerint adjuk értékül a kulcsot (nem az elemet, a kulcsot!)
        * Ha ez volt az utolsó elem, akkor hiába az elejéről szedünk ki, a vége is benne van a buliban, ezért a lastot frisíteni kell (hogy legközelebb már tudjuk, hogy üres a lista)

    ```
    Queue::first() : T
        HA head->next = end
            HIBA
        KÜLÖNBEN
            return head->next->key
	```
	
	* Itt a feltétel egy szinonímája az előző ürességvizsgálatnak

### Harmadik variáció

* Adott két sor, amiben számok vannak
* Mindkettő szigorúan monoton növekvő
* A lehető legjobb műveletigénnyel egy verembe gyűjtsd ki azokat a számokat, amik kizárólag az egyik vagy kizárólag a másik sorban voltak, de nem mindkettőben
* A veremben olyan sorrendben legyenek a számok, hogy legfelül legyen a legnagyobb, alatta a második legnagyobb, stb., legalul a legkisebb
* A két sor tartalma maradjon ugyanaz, mint a futás előtt

```
symmDiff(q1 : Queue, q2 : Queue) : Stack
    q1.add("#")
    q2.add("#")
    s : Stack
    AMÍG q1.first() != "#" és q2.first() != "#"
        VHA q1.first() < q2.first()
            s.push(q1.first())
            q1.add(q1.rem())
        VHA q1.first() > q2.first()
            s.push(q2.first())
            q2.add(q2.rem())
        VHA q1.first() = q2.first()
            q1.add(q1.rem())
            q2.add(q2.rem())
    AMÍG q1.first() != "#"
        s.push(q1.first())
        q1.add(q1.rem())
    AMÍG q2.first() != "#"
        s.push(q2.first())
        q2.add(q2.rem())
    q1.rem()
    q2.rem()
    return s
```

* Szimmetrikus differenciát kell számolnunk
* Előbb berakunk egy extremális elemet mindkét inptuba, majd az üresség helyett eme elem előfordulásáig megyünk
* Éselősen soroltam fel, tehát akkor állunk le, ha legalább az egyik "kifogyott"
* Addig is, bármelyikből is veszek ki, rögtön vissza is írom bele, illetve, ha nem a metszet eleméről van szó, a verembe is belerakom
* A végén bármelyikben is van elem, a másik már üres, tehát ezek az elemek is a verembe valók
* A legvégén kiveszem az extremális értéket