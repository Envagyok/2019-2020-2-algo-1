# Harmadik zh

## Tematika

* Vermek (05_Vermek)
    * Interfésze
    * Megvalósítsai (akár saját, akár a tanultak kis átalakítása, figyelni a pontos túlterhelésekre)
    * Helyes zárójelezések számlálóval, hierarchikusan, veremmel
    * Kifejezések kiértékelése, lengyelformára hozása, annak kiértékelése (elv, lejátszás, algoritmus)
    * Verem használata (pl. palindrom, sorrendfordítás...)
    * Akár összefuttatás is lehet
* Sorok (06_Sorok)
    * Interfésze
    * Megvalósítsai (akár saját, akár a tanultak kis átalakítása, figyelni a pontos túlterhelésekre)
    * Használata (dadogós string)
    * Összefuttatás - akár immutable módon

## Formátum

* Online
    * canvas.elte.hu
    * 2020. május 12.
    * 16 órától 16.35-ig terjedő 35 percben lesz rá egy 30 perces sáv
    * Egyszer lehet próbálkozni
    * 20 pontot lehet elérni
* Feladatok:
    * A lengyelforma mint téma biztosan elő fog fordulni
    * Biztosan lesz mind a sorokkal, mind a vermekkel kapcsolatos feladat is (esetleg ötvözve)
    * Elég valószínű, hogy bele kell nézni a tömbös és főleg a láncolt reprezentációk rejtelmeibe, tehát az előző tematikát is érdemes átolvasni
    * Az egyik feladat biztosan 3 igaz-hamis kérdés lesz, 3 pontért, amit utána még 3*2 pontért indokolni is kell - azaz  ez így összesen 9 pont lesz ez a feladat
    * A maradék 11 lehet lejátszós és/vagy algoritmusírós
    * Ha vermeken kell valamit lejátszani, akkor a vermet fektetve jelöld: s [ 1, 2, 3

## A törzsanyagon túli segédanyagok

* Konzultacio.md
* Korábbi félévek zh-inak vonatkozó feladatai
    * Vigyázat, a jelölések nagyban eltérhetnek az idei elvártaktól
* Az idei zh is fel fog kerülni a pótzh-ra gyakorlás céljából

# Ide tartozó szorgalmik

* Kisszorgalmik: 12-17