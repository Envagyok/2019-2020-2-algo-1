# Első zh

## Ihlet korábbi félévek zh-iból

* A 00_Feladatok/00_KorabbiFelevek mappába feltöltött elemek közül az alábbiak az alábbi megkötésekkel vonatkoznak az idei első zh tematikájára

### 2017.11.23. - első zh

* Fájlok:
    * zh1_20171123_fa.pdf
    * zh1_20171123_meo.pdf
* Vonatkozó feladatok:
    * 1. a)
    * 1. b)
* Megjegyzések:
    * Maximumkiválasztó rendezésről még nem volt ugyan szó, de tudni érdemes, az is egy négyzetes rendezés, mint a beszúró és a buborékrendezések

### 2018.11.26. - első zh

* Fájlok:
    * zh1_20181126_fa.pdf
    * zh1_20181126_meo.pdf
* Vonatkozó feladatok:
    * 1.
    * 2. b)
    * 2. c)
    * 2. +)
    * 3-asra is nézzünk rá
* Megjegyzések:
    * Az első feladat a) és b) része az órai anyagban bizonyítás nélkül megtett állítás igazolása
    * A c) részben a lengyelformára hozást még nem tanultunk, de a lényege, hogy egyszer megy végig az inputon
    * A 2. feladat láncolt listás, ehhez a zh-hoz még nem kell, de a műveletigény-elemzésére érdemes ránézni. Ebben a félévben még defaultból 1-től voltak indexelve a tömbök, ezt leszámítva még a "true" feltételű, aztaz végtelen ciklus lehet érdekes, de ebből returnölünk, ha l igaz lesz - márpedig igaz lesz -, tehát mégse végtelen
    * A 3-ast sem feltétlen kell még értenünk e ponton, de a konstans műveletigény volt elvárva, és jól látszik, hogy se ciklus, se rekurzió, csak alap listaelem-műveletek, azaz ez konstans műveletigény

### 2019.05.08. - első zh

* Fájlok:
    * zh1_20190508_fa.pdf
    * zh1_20190508_meo.pdf
* Vonatkozó feladatok:
    * 1.
    * 2. c)
* Megjegyzések:
    * Az első feladat jó bonyolult lett, mindenképp javasolt az értő átolvasása
    * A másodiknál elég jól el van magyarázva a megoldás, még így a tanult részletek nélkül is talán érthető
    * Mindenképp olvassuk el a 2. feladat alatti dőlt betűs rész végét, ott abszolút ide tartozó gyakori hibára hívom fel a figyelmet