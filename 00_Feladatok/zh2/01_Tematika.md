# Második zh

## Tematika

* Láncolt listák (04_LancoltListak)
    * Listák felépítése, "elemei", szemléletesen
    * Listaelemek felépítése, variációi (E1, E2, E1C, E2C), adattagok, különbségek, konstruktorok
    * Pointerek használata
    * NULL érték és nem definiált érték közti különbségek
    * Heap kezelése: new és delete, újrainicializálás, memóriaszivárgás elkerülése listamanipuláció során, különbségtétel a pointer és az általa mutatott terület között
    * Listák tulajdonságai - előnyei-hátrányai a tömbökkel szemben
    * Listák típusai, úgy mind: H, 2 és C
    * Listák típusai, úgy mind: rendezett, többszörösen láncolt, végelemes
    * Kiláncolás és beláncolás - az egyes típusok függvényében
    * Listabejárás: akár előrenézéssel, ami követhet egy vagy két pointeres módszert
    * Mikor kell nullcheck?
    * Mikor kell referencia szerinti paraméterátadás?
    * Műveletigények - szemléletesen
    * Tömbös reprezentáció - és általában, a tömbökről tanultakat és ismételjétek át
    * Maximumkiválasztó és beszúró rendezések (tömbös esettel való összehasonlítással együtt)
    * Összefuttatás - éselős típus, de lehet unióra, metszetre, különbségre vagy szimmetrikus differenciára épülő feladat is

## Formátum

* Online
    * canvas.elte.hu
    * 2020. április 21.
    * 16 órától 16.35-ig terjedő 35 percben lesz rá egy 30 perces sáv
    * Egyszer lehet próbálkozni
    * 20 pontot lehet elérni
* Feladatok:
    * Algoritmusírás
        * Tanultak alapján, vagy azokon módosítva, vagy azokhoz hasonló nehézségű, de új feladat
        * Struktogram vagy pszeudokód
        * Műveletigényt kérhetek hozzá
    * Algoritmusjavítás
        * Egy algoritmust fogok pszeudokódként megadni
        * A feladat is meg lesz adva, amit ez az algoritmus hivatott megoldani
        * Ebben kell hibákat keresni - a hiba lehet funkcionális, szintaktikai, konvencionális, és lehet csak felesleges és/vagy műveletigényt rontó kódrészlet is
        * A feladatban leírom mit kell keresni
        * A feladat a kód javítása
    * Fogalmak értelmezése
        * Vagy esszéírás vagy kvíz-szerű, a kahootos feladatokhoz hasonló várható
        * Maguk a fogalmak a honlapon elérhetők, ezért nem ezeket fogom kérdezni, hanem olyan kérdésekre kell válaszolni, amikre a fogalmak ismeretére és leginkább értésére, átlátására van szükség

## A törzsanyagon túli segédanyagok

* Konzultacio.md
* Korábbi félévek zh-inak vonatkozó feladatai
    * Vigyázat, a jelölések nagyban eltérhetnek az idei elvártaktól
* Az idei zh is fel fog kerülni a pótzh-ra gyakorlás céljából

# Ide tartozó szorgalmik

* Kisszorgalmik: 06-11