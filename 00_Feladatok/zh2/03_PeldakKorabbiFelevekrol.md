# Második zh

## Ihlet korábbi félévek zh-iból

* A 00_Feladatok/00_KorabbiFelevek mappába feltöltött elemek közül az alábbiak az alábbi megkötésekkel vonatkoznak az idei második zh tematikájára

### 2017.11.23. - első zh

* Fájlok:
    * zh1_20171123_fa.pdf
    * zh1_20171123_meo.pdf
* Vonatkozó feladat:
    * 2.
* Megjegyzések:
    * Az "fv" jelölést használtuk korábbi félévekben arra, hogy az adott függvényről közüljük, vissza fog térni valamivel, nem void - ezt most már visszatérési típussal jelezzük
    * A nagy L betű immáron konvenció szerint kicsi
    * A megoldásban az "EL" az egyszerű lista (SL) rövidítése
    * "mut" = next

### 2018.11.26. - első zh

* Fájlok:
    * zh1_20181126_fa.pdf
    * zh1_20181126_meo.pdf
* Vonatkozó feladatok:
    * 2.
* Megjegyzések:
    * Itt is érvényes, hogy inkább kis betűkkel nevezzük el a változókat és angolul próbáljunk írni (egyikért se jár persze pontlevonás, ezek csak konvenciók)

### 2019.05.08. - első zh

* Fájlok:
    * zh1_20190508_fa.pdf
    * zh1_20190508_meo.pdf
* Vonatkozó feladatok:
    * 2.
* Megjegyzések:
    * A 2. zh-hoz a vermet még nem kell tudni, de mivel itt csak az interfészét használjuk és azt ezen a ponton már vettük, érdemes ezt a feladatot is átolvasni a készüléshez
    * A "=" ebben a félévben ":=" és a "==" helyett "="-t írunk
    