# Láncolt listák aritmetikai (tömbös) ábrázolása (reprezentációja)

* Maga a láncolt lista amellett, hogy egy interfész, egyben egy implementációt is jelent (láncolt megvalósítás)
* De a lista interfészt (azazhogy egy iterált és lineáris adatszerkezet, melynek van első eleme és tudunk rajta lépegetni, beszúrni egy elem mögé, kiláncolni egy elem mögül ideális műveletigényekkel) persze tömbökkel is szimulálhatjuk, így:
	* Legyen egy tömbünk, amiben párok vannak. A pár első eleme maga a listaelem, ha úgy tetszik a kulcs. A másik elem, pedig a "next", bár itt ez nem egy pointer, hanem egy tömbindex: ennyiedik helyen lesz a következő elem
	* Az utolsó elem index komponense a -1 (vagy ha 1-től indexelnénk, lehetne 0; esetleg lehet 0-tól indexelésnél n, 1-től indexelésnél n+1)
	* Tartozik még egy adattag a típushoz, az első elem tömbbeli indexe, ezt most s-sel jelölöm (start)
* Azért jó ez a fajta megvalósítás az ArrayListhez/vectorhoz képest, mert ha a lista elejéről törlünk, nem kell balra shiftelni minden elemet, csak átállítjuk az első elem indexét
* Persze a lista bejárása így is viszonylag drága, mert mindig ki kell olvasni a következő indexeket (de nagyságrendben lineáris)
* Ha új elemet kell beszúrni, azt tehetjük az elejére konstans idővel; tehetjük egy adott elem után - ha az elem indexe megvan, onnan már ez is konstans idő; valamint tehetjük a végére, amihez végig kell járni a listát
	* Na de, az nem evidens, hogy hova is tudjuk berakni ezt az elemet - egyáltalán van-e szabad hely
	* A tömb fizikailag első üres helyét kellene megtalálni, ami potenciálisan lineáris, azaz az összes konstans beszúrást lerontja lineárisra
	* Épp ezért számon tartunk még egy tömböt, az üres elemek "listáját", ugyanúgy -1-gyel terminálva és fizikailag ugyanabban a tömbben tárolva, hiszen eme két "lista" összesített hossza mindig a tároló tömb kapacitása lehet
    * Emellett kelleni fog még egy indexváltozó, a szabad elemek listájának első eleme, jelöljük ezt es-sel (empty start)
	* Ekkor ha a lista elejére akarunk szúrni, konstans időben megtaláljuk az es alapján az első szabad helyet, ide beszúrhatjuk a kulcsot, majd az es-t ennek az elemnek a nextjére állíthatjuk. Az eredeti es elem nextje lesz az eddigi s, és s-t pedig ráállítjuk erre az elemre. Ez konstans műveletigény
* Szúrhatunk konstans időben a lista végére is
	* A konstans időhöz még az utolsó foglalt listaelem indexét is tárolni kell, legyen a neve e (end)
    * Ekkor fogjuk megint az se indexet, se-t átállítjuk annak nextjére, az eddigi e-re beállítjuk a régi se-t, majd e-t a régi se-re állítjuk
* Tehát: kell a tömb, az üres tömb (ami lehet fizikailag egy tömbben is!), kell a két tömb eleje és a kitöltött elemek tömbjének vége, így a megfelelő műveletek konstans műveletigénnyel működhetnek
	* Ez amúgy így konkrétan a "végelemes lista" implementációja
* Erre nincs egyféle "kanonizált" megoldás, hanem használatieset-függő a megvalósítás mikéntje
* Felmerülhet a kérdés, miért lehetnek ezek az indexek így össze-vissza, miért nem 0-tól kezdjük a számolást
    * Alapvetően onnan kezdjük, de ahogy random szedünk ki és rakunk be elemeket, a konstans műveletigényért cserébe összekeveredhet a sorrend
* Példa

```
1-->2-->3
```

```
s = 2
e = 4
es = 3
```

| index | 0 | 1  | 2 | 3 | 4  |
|-------|---|----|---|---|----|
| érték |2,4|*,-1|1,0|*,1|3,-1|
