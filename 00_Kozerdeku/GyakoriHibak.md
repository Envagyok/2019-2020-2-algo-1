# Gyakori hibák

* Ide gyűjtök gyakran elkövetett hibákat, akár zh-ra, akár vizsgára jó lehet átfutni ezeket
* Nyugodtan bővítsétek Ti is!

## Számok

* Ha egy biztosan természetes szám ellentettjét veszed, az már nem természetes szám, ezt jelölni kell a típusában
* Tegyük fel, hogy n : N, és n = 5. A való életben ekkor n/2 = 2. A mi modellünkben ez 2,5, tehát ha természetes (egész) számként akarod használni, kasztolni kell pl. az alsóegészrész(2,5) = 2 függvény használatával

## Tömbök

* Ha t : T[n], azaz n méretű, T-ket tartalmazó tömb, akkor 0..n-1-ig vagy 0..t.length-1-ig van indexelve
* Ha 1-től n-ig akarod, akkor t/1 : T[n]-ként kell deklarálni
* Konkrét példákban ha meg van adva mi van egy tömbben, sose T-t írj, hanem a megadott típust (a "T" nem a tömb rövidítése, hanem a típusé - attól lesz tömb, hogy szögleteszárójel-pár van mögötte)
* Tömböt nem adunk át referencia szerint, ha csak egy adott értékét akarjuk változtatni
* Teljesen valid "t : T[]"-ként átadni egy tömböt, de akkor t méretére biztosan csak a t.lenght-tel és nem n-nel tudsz hivatkozni
* Néha elfelejtitek a tömböt kinullázni az algoritmus elején (ez feladatfügő, nem is mindig kell - de mindig gondolj rá)

## Pointerek

* Pointernek nincs adattagja, tehát ha p : E1*, akkor nincs p.next, csak p->next
* Pointer alapértelmezésben nem NULL, hanem definiálatlan (kivéve az újonnan létrejött láncoltlista-elem next mutatója - de ez a konstruktor miatt van így)

## Láncolt listák

* Egy frissen, az üres konstruktorral létrehozott E1 kulcsa nem definiált, azaz nem lehet kiolvasni mielőtt értéket ne adnánk neki, de a nextje szigorúan NULL és felesleges, azaz hibának számít külön NULL-ra inicializálni

## Vezérlési szerkezetek

* A FOR i := n to 1 egy üres számlálós ciklus, mert a "to" implicite az eggyel növekedést jelenti. A keresett szintaktika a FOR i := n down to 1 jelen esetben
* Ha egy többágú elágazásban egy eset nincs lekezelve, akkor az nem automatikus SKIP, hanem hiba (az ún. ABORT program fut olyankor)
* Ha egy többágú elágazásban két eset feltétele átfed, akkor random fut le az egyik és csak az egyik (nem szükségképpen a "legbaloldalibb")
* breaket és continuet nem ismerünk ebben amodellben

## Függvények

* Egy függvényben akkor és csak akkor legyen "return x" ha a függvény fejében is feltünteted az x típuásnak megfelelő visszatérési típust
* Nem gond, ha egy függvény neve megegyezik egy változó nevével (pl. egy az adott függvény lokális váltózójáéval) - de ilyen esetben fokozott körültekintéssel, végig következetesen használjuk a két elemet

## Matek

* Ha egy lim(n->végtelen) f(x) függvény után írjátok, hogy "tovább egyenlő", akkor ne felejtsétek el a limes jelet újra kirakni. Ha ez elmarad, akkor az már maga a határérték lesz

## Műveletigény

* Minimális műveletigény esetén előfordul az az érvelés, hogy pl. a buborékrendezésben az összehasonlítások száma minimálisan 0, mert ha üres az input, akkor nem kell összehasonlítani. Ezzel az a probléma, hogy az input méretének függvényében számoljuk a műveletigényt, tehát n = 0-ra f(0) = 0 akármi is lehet. Azt kell mondani, hogy vajon van-e rögzített n-re olyan n méretű tömb, ahol kisebb ez a szám. Kontrasztképp, ha a cserék számát nézzük, ott akármilyen n-re tudok mondani olyan tömböt (rendezett tömb), ahol a cserék száma 0 lesz