# Helyes zárójelezés

## Számlálóval

* Egyszerű eset, ha s eleme { '(', ')' }*, azaz s egy csupa nyitó és csukó zárójelekből álló string. Döntsük el, helyesen van-e zárójelezve
* Mi kell? count(s, ’(’) = count(s, ’)’) és minden i eleme [1..n]: count(s[1..i], ’(’) >= count(s[1..i], ’)’)
	* Azaz, összesen ugyanannyi nyitó, mint csukó zárójel legyen, de a prefixekben lehet több (ezek a megnyitott, de még nem becsukott zárójelek). Fordítva nem lehet
* Ezt pl. egy számlálóval tudjuk könnyedén ellenőrizni
	* A következőkben a zárójelek fölé írom, épp ott hogy áll a számlálás. Nyitónál növelem, csukónál csökkentem. Akkor jó, ha
		* Sose megy 0 alá
		* A végén épp 0
* Első példa - helyes zárójelezés

	```
	1 2 1 2 3 2 3 2 1 0
	( ( ) ( ( ) ( ) ) )
	```
	
* Második példa - helytelen zárójelezés

	```
	1 2 1 2 3 2 3 2 3 2
	( ( ) ( ( ) ( ) ( )
	```

* Megj.: ha a zárójeleken kívül más karakter is lehetne az inputban, az se gond, ezeket átugorjuk
* Megj.: Két számlálóval is megoldható a feladat - az egyik számlálja a nyitókat, a másik a zárókat. Az itt ismertetett az egyszerűbb megoldás