# Helyes zárójelezés

## Veremmel

* Itt most a vermet, és a műveleteit adottnak tekintjük, nem kell (nem is szabad) az implementációjába belemenni, az interfészben megadott műveleteket használhatjuk
* Legyen adott egy "rendes" matematikai kifejezés, mindenféle egyéb karakterekkel (műveletek, paraméterek, ...)
* Döntsük el, helyesen van-e zárójelezve és írjuk ki a nyitó-csukó-párok indexeit a becsukás sorrendjében (formázás nem érdekes, és nem baj, ha egy helytelenül zárójelezett kifejezés első néhány zárójelét kiírjuk)
    * A kifejezések helyességét úgy amúgy nem ellenőrizzük
* Példa:
	* Bemenet: ( ( a + b ) / ( 2 - x ) ) ^ ( 5 * z )
	* Kimenet: Helyes; (1, 5) (7, 11) (0, 12) (14, 18)

```
checkBalancedParentheses(s : InStream, &out : OutStream) : L
	v : Stack
	out := <>
	i := 0
	ch := s.read() // ch : Ch segédváltozó
	AMÍG ch != EOF
		HA ch = ’(’
			v.push(i)
		KÜLÖNBEN HA ch = ’)’
			HA v.isEmpty()
				return false
			KÜLÖNBEN
				out.print(v.pop())
				out.print(i)
		KÜLÖNBEN
		    SKIP
		ch := s.read()
		i := i+1
	return v.isEmpty()
```

* A függvény a helyességgel tér vissza, mellékhatásként kiír az átadott kimeneti folyamra (pl. konzolra)
* Előreolvasunk, karakterenként feldolgozunk
    * Direkt bemeneti folyam a bemenő paraméter, ezt nem lehet indexelni, nem lehet visszalépni már beolvasott elemre, és a hosszát se tudjuk lekérdezni. Csak elemenkénti feldolgozásra van lehetőség
    * Azt is "utólag" tudjuk meg, sikerült-e az olvasás. Akkor és csak akkor nem sikerült, ha üres volt az input. Ha az utolsó olvasás után a beolvasott karakter az EOF (end of file) konstans, akkor nem sikerült. Ha egy ilyet olvasunk, utána már ne olvassunk többet. Ha eleve üres az input, az első olvasás EOF-ot fog produkálni. Direktben az InStream ürességét nem tudjuk lekérni
* Ha nyitót olvasunk, berakjuk a verembe (kb. mint a számláló növelése)
* Ha csukót, kivesszük, ami a számláló csökkentését jelenti. Az, hogy a számláló 0, azzal ekvivalens, ha a verem üres (és minden zárójel be van csukva és mégis csukót olvasunk)
* Maga a verem azért kell, mert ki akarjuk írni a sorszámokat (azaz tulajdonképpen a bejárás állapotait akarjuk elmenteni adott sorrendben)
* A végén akkor volt helyes, ha épp kiürült a verem (azaz számlálós analógiával: ha nem pozitív a számláló, nincs megnyitott, de nem bezárt zárójel)